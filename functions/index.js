

  'use strict';

const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
const gmailEmail = encodeURIComponent(functions.config().gmail.email);
const gmailPassword = encodeURIComponent(functions.config().gmail.password);
const mailTransport = nodemailer.createTransport(
    `smtps://${gmailEmail}:${gmailPassword}@smtp.gmail.com`);

const gcs = require('@google-cloud/storage')();
const sharp = require('sharp');
const _ = require('lodash');
const path = require('path');
const os = require('os');
//firebase functions:config:set gmail.email="billibaby5@gmail.com" gmail.password="chyle1992"

const APP_NAME = 'Neu Fit';
exports.sendEmail = functions.auth.user().onCreate(
  event => {

    const post = event.data;
    const email = post.email;
    return sendEmail(email);

  })

	function sendEmail(email) {
        var senTo = 'billibaby5@gmail.com';
		const mailOptions = {
	    to: senTo
	  };

	  mailOptions.subject = `New User Registration`;
	  mailOptions.text = `Email: ${email}`;
	  return mailTransport.sendMail(mailOptions).then(() => {
	    console.log('user Registration');
	  });
	}
exports.sendWelcomeEmail = functions.auth.user().onCreate(
  event => {

    const post = event.data;
    const email = post.email;
    return sendEmail(email);

  })
	function sendWelcomeEmail(email) {
        var senTo = email;
		const mailOptions = {
	    to: senTo
	  };

	  mailOptions.subject = `New User Registration`;
	  mailOptions.text = `Welcome!!! Congratulations for taking your first step of your fitness journey`;
	  return mailTransport.sendMail(mailOptions).then(() => {
	    console.log('sendWelcomeEmail');
	  });
	}


exports.generateThumbnail = functions.storage.object('user-profiles/{pushId}').onChange(event => {

  const object = event.data; // The Storage object.

  console.log(object);

  const fileBucket = object.bucket; // The Storage bucket that contains the file.
  const filePath = object.name; // File path in the bucket.
  const contentType = object.contentType; // File content type.
  const resourceState = object.resourceState; // The resourceState is 'exists' or 'not_exists' (for file/folder deletions).
  const metageneration = object.metageneration; // Number of times metadata has been generated. New objects have a value of 1.

  const SIZES = [64, 256, 512]; // Resize target width in pixels

  if (!contentType.startsWith('image/') || resourceState == 'not_exists') {
    console.log('This is not an image.');
    return;
  }

  if (_.includes(filePath, '_thumb')) {
    console.log('already processed image');
    return;
  }


  const fileName = filePath.split('/').pop();
  const bucket = gcs.bucket(fileBucket);
  const tempFilePath = path.join(os.tmpdir(), fileName);

  return bucket.file(filePath).download({
    destination: tempFilePath
  }).then(_ => {
    console.log('Thumbnail created locally.')
    metadata.isThumb = true  ;// We add custom metadata
    const options = {
        destination: tempFilePath,  // Destination is the same as original
        metadata: { metadata: metadata }
    };
    // We overwrite the (bigger) original image but keep the path
    return bucket.upload( options);
});

  //   .then(() => {
  //
  //   _.each(SIZES, (size) => {
  //
  //     let newFileName = `${fileName}_${size}_thumb.png`
  //     let newFileTemp = path.join(os.tmpdir(), newFileName);
  //     let newFilePath = `thumbs/${newFileName}`;
  //
  //     sharp(tempFilePath)
  //       .resize(size, null)
  //       .toFile(newFileTemp, (err, info) => {
  //
  //         bucket.upload(newFileTemp, {
  //           destination: newFilePath
  //         });
  //
  //       });
  //
  //   })
  // })
})
