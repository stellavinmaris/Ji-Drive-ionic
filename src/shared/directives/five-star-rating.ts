import { AfterViewInit, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'five-star-rating',
  template: `
    <ion-icon [name]="firstStarClass"></ion-icon>
    <ion-icon [name]="secondStarClass"></ion-icon>
    <ion-icon [name]="thirdStarClass"></ion-icon>
    <ion-icon [name]="fourthStarClass"></ion-icon>
    <ion-icon [name]="fifthStarClass"></ion-icon>
    <span *ngIf="showRatingValue">{{ rating }}</span>
  `
})
export class FiveStarRatingComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {

  @Input()
  rating: any = '0';

  @Input()
  showRatingValue: boolean = true;

  private firstStarClass: string = 'star-outline';
  private secondStarClass: string = 'star-outline';
  private thirdStarClass: string = 'star-outline';
  private fourthStarClass: string = 'star-outline';
  private fifthStarClass: string = 'star-outline';

  constructor(
    // private _element: ElementRef
  ) {

  }


  ngOnInit() {
  }

  ngAfterViewInit() {
  }


  ngOnDestroy() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.firstStarClass = this.starRating(1);
    this.secondStarClass = this.starRating(2);
    this.thirdStarClass = this.starRating(3);
    this.fourthStarClass = this.starRating(4);
    this.fifthStarClass = this.starRating(5);
  }

  starRating(star:number) {
    if (star < parseFloat(this.rating) + 1.00 && star > parseInt(this.rating)) {
      return "star-half"
    } else if (star <= parseInt(this.rating)) {
      return "star"
    } else {
      return "star-outline"
    }
  }

}
