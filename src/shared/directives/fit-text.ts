import { AfterViewInit, Directive, ElementRef, HostListener, Input, OnDestroy, OnInit } from '@angular/core';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { Platform } from "ionic-angular";


@Directive({
  selector: '*[fit-text]'
})
export class FitTextDirective implements OnInit, AfterViewInit, OnDestroy {


  @Input()
  compressor: number = 1.02;

  @Input()
  minFontSize: number = 10;

  @Input()
  maxFontSize: number = 100;

  @Input()
  growText: boolean = true;

  @Input()
  shrinkText: boolean = true;


  private resizeSubscription;
  private originalSize;

  constructor(
    private _element: ElementRef,
    private _platform: Platform
  ) {
  }


  ngOnInit() {
  }

  ngAfterViewInit() {
    if (!this.originalSize) {
      this.originalSize = parseFloat(window.getComputedStyle(this._element.nativeElement, null).getPropertyValue('font-size'));
    }
    this.resizeSubscription = this._platform.resize.subscribe((res) => {
      this.resizer();
    });
    this.resizer();
  }

  ngOnDestroy() {
    this.resizeSubscription.unsubscribe();
  }

  resizer() {
    let size = Math.max(
      Math.min(
        this._element.nativeElement.parentElement.offsetWidth / (this.compressor * 10), this.maxFontSize
      ), this.minFontSize
    );
    this.findLowestChild(this._element.nativeElement, (element) => {
      console.log(this.originalSize,size);
      if (this.originalSize <= size && this.growText) {
        element.style.fontSize = size + 'px';
      } else if (this.originalSize >= size && this.shrinkText) {
        element.style.fontSize = size + 'px';
      } else {
        element.style.fontSize = this.originalSize + 'px';
      }
    });
  };

  findLowestChild(node, cb) {
    if (node.nodeType !== 3 && node.children.length > 0) {
      this.findLowestChild(node.children[0], cb);
    } else {
      cb(node);
    }
  }


}
