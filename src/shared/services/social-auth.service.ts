import {Http, Response, RequestOptionsArgs, URLSearchParams} from '@angular/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../bases/services/BaseService';
import { HttpSettingsService } from './HttpSettingsService';
import { SessionService } from './session.service';
import { AuthTokenService } from './authtoken.service';
import { ListResponse } from '../bases/models/ListResponse';
import { Auth } from '../models/Auth';
import { Observable } from 'rxjs/Observable';
import { InAppBrowser, InAppBrowserObject, InAppBrowserOptions } from "@ionic-native/in-app-browser";
import { SettingsService } from "./SettingsService";
import { Platform } from "ionic-angular";
@Injectable()

export class SocialAuthService extends BaseService {

  public _basePath = 'get-token/';
  public browser: InAppBrowserObject;
  public options: InAppBrowserOptions = {
    location: 'no',
    shouldPauseOnSuspend: 'yes',
    disallowoverscroll: 'yes',
    toolbar: 'yes',
    enableViewportScale: 'yes',
    clearcache: 'yes',
    allowInlineMediaPlayback: 'yes',
    presentationstyle: 'pagesheet',
    transitionstyle: 'crossdissolve'
  };

  constructor(public http: Http,
              public _httpSettings: HttpSettingsService,
              public _settings: SettingsService,
              public _sessionService: SessionService,
              public _inAppBrowser: InAppBrowser,
              public _platform: Platform,
              private _authToken: AuthTokenService
  ) {
    super(http, _httpSettings);
    let hash = document.location.hash;
    let token = hash.replace('#api-token=', '');
    if (token.length > 10) {
      this._authToken.setToken(token);
      setTimeout(() => {
        this._sessionService.actionLoggedIn();
      }, 100);
      document.location.hash = '';
    }
  }

  listMap(res: Response): ListResponse {
    let toReturn = <ListResponse>res.json();
    for (let num in toReturn.results) {
      if (toReturn.results.hasOwnProperty(num)) {
        toReturn.results[num] = new Auth(toReturn.results[num]);
      }
    }
    return toReturn;
  }

  singleMap(res: Response): Auth {
    return new Auth(res.json());
  }

  showBrowser(OAuthType) {
    return this._inAppBrowser.create(this._settings.getProtocol() + "://" + this._settings.getDomain() + '/login/'+OAuthType+'/',
      '_blank', this.options
    );
  }

  facebookLogin() {
    if(this._platform.is('core') || this._platform.is('mobileweb')) {
      return this.facebookLoginDesktop();
    }
    let token;
    this.browser = this.showBrowser('facebook');
    return Observable.create((subscriber) => {
      let loadstartSubscription = this.browser.on('loadstart').subscribe((res) => {
        let token_test = res.url.replace(/#.*?$/, '').replace(/^.*?api-token=(.*?)$/, "$1");
        if (token_test.length > 2 && !token_test.match(/^http/)) {
          token = token_test;
          this._authToken.setToken(token);
          this._sessionService.actionLoggedIn();
          subscriber.next(token);
          this.browser.close();
        }
      });
      let exitSubscription = this.browser.on('exit').subscribe(() => {
        exitSubscription.unsubscribe();
        loadstartSubscription.unsubscribe();
        if (typeof token === 'undefined') {
          subscriber.error({detail: "Authentication was canceled."});
        }
        subscriber.complete();
      });
    });
  }

  facebookLoginDesktop() {
    let token;
    let OAuthType = 'facebook';
    let url = this._settings.getProtocol() + "://" + this._settings.getDomain() + '/login/'+OAuthType+'/';
    document.location.href = url;
  }

  googleLoginDesktop() {
    let token;
    let OAuthType = 'google-oauth2';
    let url = this._settings.getProtocol() + "://" + this._settings.getDomain() + '/login/'+OAuthType+'/';
    document.location.href = url;
  }

  googleLogin() {
    if(this._platform.is('core') || this._platform.is('mobileweb')) {
      return this.googleLoginDesktop();
    }
    let token;
    this.browser = this.showBrowser('google-oauth2');
    return Observable.create((subscriber) => {
      let loadstartSubscription = this.browser.on('loadstart').subscribe((res) => {
        let token_test = res.url.replace(/#.*?$/, '').replace(/^.*?api-token=(.*?)$/, "$1");
        if (token_test.length > 2 && !token_test.match(/^http/)) {
          token = token_test;
          this._authToken.setToken(token);
          this._sessionService.actionLoggedIn();
          subscriber.next(token);
          this.browser.close();
        }
      });
      let exitSubscription = this.browser.on('exit').subscribe(() => {
        console.log('exit', token);
        exitSubscription.unsubscribe();
        loadstartSubscription.unsubscribe();
        if (typeof token === 'undefined') {
          subscriber.error({detail: "Authentication was canceled."});
        }
        subscriber.complete();
      });
    });
  }


  public handleError (error: Response) {
    let json = error.json();
    // var toReturn = 'Server error';
    let toReturn = json;
    if (json.hasOwnProperty('error')) {
      toReturn = json.error;
    }
    if (json.hasOwnProperty('detail')) {
      toReturn = json.detail;
    }

    return Observable.throw(toReturn);
  }
}
