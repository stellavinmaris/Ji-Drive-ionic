import { Injectable } from '@angular/core';
import { BaseService } from '../../shared/bases/services/BaseService';
import {ListResponse} from "../bases/models/ListResponse";
import {Auth} from "../models/Auth";



@Injectable()
export class UserInvitationCodeService extends BaseService {

  public _basePath = 'user-invitation-codes/';
}