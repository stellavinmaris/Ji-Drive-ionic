import { Injectable } from '@angular/core';
import { ENV } from '../constant/env';
import { StorageService } from "./storage.service";
import { Observable } from "rxjs/Observable";

@Injectable()

export class FirstRunService {

  constructor(
    private _storageService: StorageService
  ) {

  }

  checkForFirstRun() {
    return Observable.create((subscriber) => {
      this._storageService.get('first-run-done').then((value) => {
        if (value) {
          subscriber.next(false);
        } else {
          subscriber.next(true);
        }
        subscriber.complete();
      });

      return () => {
        //cleanup
      };
    });
  }

  setFirstRun(value) {
    return this._storageService.set('first-run-done', value);
  }


}
