import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/login/home';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {AngularFireModule} from "angularfire2";
import {AngularFireAuthModule} from "angularfire2/auth";
import {ProductsPage} from "../pages/products/products";
import {ResetPasswordPage} from "../pages/reset-password/reset-password";
import {RegisterPage} from "../pages/register/signup";
import {AddGymPage} from "../pages/add-gym/add-gym";
import {AngularFireDatabase} from "angularfire2/database";
import {ListGymPage} from "../pages/list-gym/list-gym";
import {GymPage} from "../pages/view-gym/view-gym";
import {HttpModule} from "@angular/http";
import {FitnessCentersPage} from "../pages/fitness-centers/fitness-centers";
import {WellnessCenterPage} from "../pages/wellness-centers/wellness-center";
import {ProfilePage} from "../pages/profile/profile";
import {SessionService} from "../shared/services/session.service";
import {AuthService} from "../shared/services/auth.service";
import {RegisterService} from "../shared/services/register.service";
import {AuthTokenService} from "../shared/services/authtoken.service";
import {SettingsService} from "../shared/services/SettingsService";
import {HttpSettingsService} from "../shared/services/HttpSettingsService";
import {DashboardPage} from "../pages/dashboard/dashboard";

export const firebaseConfig = {
    apiKey: "AIzaSyC6A-d2VJwK164tyV-YiA6vRBFb1xUkt7s",
    authDomain: "fir-project-e5251.firebaseapp.com",
    databaseURL: "https://fir-project-e5251.firebaseio.com",
    projectId: "fir-project-e5251",
    storageBucket: "gs://fir-project-e5251.appspot.com/",
    messagingSenderId: "902921514274"
  };

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ProductsPage,
    ResetPasswordPage,
    RegisterPage,
    AddGymPage,
    ListGymPage,
    GymPage,
    FitnessCentersPage,
    WellnessCenterPage,
    ProfilePage,
    DashboardPage,


  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ProductsPage,
    ResetPasswordPage,
    RegisterPage,
    AddGymPage,
    ListGymPage,
    GymPage,
    FitnessCentersPage,
    WellnessCenterPage,
    ProfilePage,
    DashboardPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireDatabase,
      SessionService,
      AuthService,
      RegisterService,
      AuthTokenService,
      SettingsService,
      HttpSettingsService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
