import { Component, ViewChild } from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {AngularFireAuth} from "angularfire2/auth";
import {ProductsPage} from "../pages/products/products";
import {ProfilePage} from "../pages/profile/profile";
import * as firebase from "firebase";
import {HomePage} from "../pages/login/home";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  //TODO: return login page
  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;
  admin: Array<{title: string, component: any}>;
  settings: Array<{title: string, component: any}>;
  public currentUser: any = [];
  public rawList= [];
  public userResponse: any = [];
  public CurrentUserEmail: any;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              private auth: AngularFireAuth,
              ) {
    this.initializeApp();


    // used for an example of ngFor and navigation
    this.pages = [

      // {title: 'View Vehicles', component: ProductsPage },
      //   { title: 'Edit Profile', component: SignupPage },
    ];


      this.settings = [

    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  editProfile(){
        this.nav.setRoot(ProfilePage);
      }






}
