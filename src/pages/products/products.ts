import { Component } from '@angular/core';
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms';
import { NavController } from 'ionic-angular';
import { ResetPasswordPage } from '../reset-password/reset-password'
import {GymPage} from "../view-gym/view-gym";

@Component({
  selector: 'page-products',
  templateUrl: 'products.html'
})
export class ProductsPage {
  loginForm: FormGroup;
  email: AbstractControl;
  password: AbstractControl;
  error: any;
  resetPasswordPage = ResetPasswordPage;//Added reset password page

  constructor(private navCtrl: NavController,
           ) {

  }


  goTogym (){
    this.navCtrl.push(GymPage);
  }


}
