import { Component } from '@angular/core';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { NavController } from 'ionic-angular';
import {ListGymPage} from "../list-gym/list-gym";

@Component({
  selector: 'wellness-center',
  templateUrl: 'wellness-center.html'
})
export class WellnessCenterPage {

  email: AbstractControl;
  password: AbstractControl;
  error: any;


  constructor(private navCtrl: NavController,
              private fb: FormBuilder,) {

  }

  goToGym(){
    this.navCtrl.push(ListGymPage);
  }


}
