import { Component } from '@angular/core';
import {Loading, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';

import {GymPage} from "../view-gym/view-gym";


@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,)
  {

  }

  goTogym (){
    this.navCtrl.setRoot(GymPage);
  }






}
