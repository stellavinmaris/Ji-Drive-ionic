import { Component } from '@angular/core';
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms';
import { NavController } from 'ionic-angular';
import { ResetPasswordPage } from '../reset-password/reset-password'
import {ListGymPage} from "../list-gym/list-gym"; //Added reset password page

@Component({
  selector: 'fitness-centers',
  templateUrl: 'fitness-centers.html'
})
export class FitnessCentersPage {
  loginForm: FormGroup;
  email: AbstractControl;
  password: AbstractControl;
  error: any;
  resetPasswordPage = ResetPasswordPage;//Added reset password page

  constructor(private navCtrl: NavController,
              private fb: FormBuilder,) {

  }

  goToGym(){
    this.navCtrl.push(ListGymPage);
  }


}
