import { Component } from '@angular/core';
import {Loading, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';

import {GymPage} from "../view-gym/view-gym";


@Component({
  selector: 'page-list-gym',
  templateUrl: 'list-gym.html'
})
export class ListGymPage {





  constructor(public navCtrl: NavController,
              public navParams: NavParams,)
  {

  }



  goTogym (){
    this.navCtrl.setRoot(GymPage);
  }






}
