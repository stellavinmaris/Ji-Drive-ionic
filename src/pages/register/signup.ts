import { Component } from '@angular/core';
import { Loading, LoadingController, NavController, ToastController, AlertController } from 'ionic-angular';
import { UserRegister } from '../../shared/models/User';
import { RegisterService } from '../../shared/services/register.service';
import {HomePage} from "../login/home";
import {SessionService} from "../../shared/services/session.service";
import {AuthService} from "../../shared/services/auth.service";


@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class RegisterPage {
  public user: UserRegister = new UserRegister({});
  private loadingController: Loading;
  private loading: boolean = false;

  constructor(private _nav: NavController,
              private _sessionService: SessionService,
              private _authService: AuthService,
              private _registerService: RegisterService,
              private loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController) {

  }

  ionViewDidEnter() {
  }

  showLoading() {
    this.loadingController = this.loadingCtrl.create({
      content: `Signing Up`
    });
    this.loadingController.present();
  }

  hideLoading() {
    this.loadingController.dismiss();
  }


  register() {
    let tester = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-?\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;

    let valid = false;
    if(this.user.email.length > 254 || this.user.email.length < 5) {
      valid = false;
    } else {
      valid = tester.test(this.user.email);
    }

    if (!valid) {
      this.toastCtrl.create({
        message: 'Please enter a valid email address.',
        duration: 3000,
        position: 'middle'
      }).present();
    } else {
      this.showLoading();
      this.loading = true;
      this._registerService.post(JSON.stringify(this.user))
        .subscribe((res) => {
            this.alertCtrl.create({
              title:'Thanks for Registering!',
              message: "Welcome to Car Hire",
              buttons: [
                {
                  text: 'OK',
                  handler: () => {
                    this._nav.setRoot(HomePage);
                  }
                }
              ]
            }).present();
            this.hideLoading();
             this.loading = false;

          },
          (errorMsg) => {
            this.loading = false;
            this.hideLoading();
            let text = '';
            for (let field in errorMsg) {
              if (errorMsg.hasOwnProperty(field)) {
                if (field === 'username') {
                  if (errorMsg[field] === 'Username already exists.') {
                    text = text + "The provided email address has already been used to register.\n";
                  } else {
                    text = text + field + ': ' + errorMsg[field] + "\n";
                  }
                } else {
                  text = text + field + ': ' + errorMsg[field] + "\n";
                }
              }
            }
            if (errorMsg.hasOwnProperty('non_field_errors')) {
              text = text + errorMsg.non_field_errors;
            }
            this.toastCtrl.create({
              message: text,
              duration: 5000,
              position: 'middle'
            }).present();
          });
    }  }

  goToLogin() {
    this._nav.setRoot(HomePage);
  }

}
