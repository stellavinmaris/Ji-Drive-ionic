import { Component } from '@angular/core';
import { Loading, LoadingController, NavController, ToastController } from 'ionic-angular';
import { SessionService } from '../../shared/services/session.service';
import { AuthService } from '../../shared/services/auth.service';
import { AuthUser } from '../../shared/models/AuthUser';
import { StatusBar } from '@ionic-native/status-bar';
import { SettingsService } from '../../shared/services/SettingsService';
import {RegisterPage} from "../register/signup";
import {DashboardPage} from "../dashboard/dashboard";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    loading: boolean = false;
    public user: AuthUser = new AuthUser({});
  private loadingController: Loading;
  constructor(private statusBar: StatusBar,
              private _nav: NavController,
              private _sessionService: SessionService,
              private _authService: AuthService,
              private loadingCtrl: LoadingController,
              private _toastCtrl: ToastController,
              private _settingsService: SettingsService,

  ) {


  }

  ionViewDidEnter(){
  }

  showLoading(){
    this.loadingController = this.loadingCtrl.create({
      content: `Logging in`
    });
    this.loadingController.present();
  }

  hideLoading(){
    this.loadingController.dismiss();
  }

  showToast(msg){
    let toast = this._toastCtrl.create({
      message: msg,
      duration: 3000,
      // position: 'top'
    });
    toast.present();
  }

  login(){
    this.loading = true;
    this.showLoading();
    this._authService.login(JSON.stringify(this.user)).subscribe((res) => {
        // this.loading = false;
        this.hideLoading();
        this._nav.setRoot(DashboardPage);
      },
      (errorMsg) => {
      this.hideLoading();
        this.loading = false;
        console.log(errorMsg);
        if (errorMsg.hasOwnProperty('non_field_errors')) {
          this.showToast(errorMsg.non_field_errors);
        } else if (errorMsg.hasOwnProperty('detail')) {
          this.showToast(errorMsg.detail);
        } else {
          let message = '';
          if (typeof errorMsg === 'string') {
            message = errorMsg;
          } else {
            for (let field in errorMsg) {
              if (errorMsg.hasOwnProperty(field)) {
                message = message + ' ' + field + ': ' + errorMsg[field];
              }
            }
          }
          this.showToast(message);
        }
      }
    );
  }

  goToRegister(){
    this._nav.push(RegisterPage);
  }

  goToForgotPassword(){
    // this._nav.push(ForgotPasswordPage);
  }

  typed($event: KeyboardEvent) {
    if ($event.keyCode === 13) {
      this.login();
    }

  }

}

