webpackJsonp([0],{

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_services_session_service__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_services_auth_service__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_models_AuthUser__ = __webpack_require__(677);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_services_SettingsService__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__register_signup__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__dashboard_dashboard__ = __webpack_require__(315);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HomePage = (function () {
    function HomePage(statusBar, _nav, _sessionService, _authService, loadingCtrl, _toastCtrl, _settingsService) {
        this.statusBar = statusBar;
        this._nav = _nav;
        this._sessionService = _sessionService;
        this._authService = _authService;
        this.loadingCtrl = loadingCtrl;
        this._toastCtrl = _toastCtrl;
        this._settingsService = _settingsService;
        this.loading = false;
        this.user = new __WEBPACK_IMPORTED_MODULE_4__shared_models_AuthUser__["a" /* AuthUser */]({});
    }
    HomePage.prototype.ionViewDidEnter = function () {
    };
    HomePage.prototype.showLoading = function () {
        this.loadingController = this.loadingCtrl.create({
            content: "Logging in"
        });
        this.loadingController.present();
    };
    HomePage.prototype.hideLoading = function () {
        this.loadingController.dismiss();
    };
    HomePage.prototype.showToast = function (msg) {
        var toast = this._toastCtrl.create({
            message: msg,
            duration: 3000,
        });
        toast.present();
    };
    HomePage.prototype.login = function () {
        var _this = this;
        this.loading = true;
        this.showLoading();
        this._authService.login(JSON.stringify(this.user)).subscribe(function (res) {
            // this.loading = false;
            _this.hideLoading();
            _this._nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__dashboard_dashboard__["a" /* DashboardPage */]);
        }, function (errorMsg) {
            _this.hideLoading();
            _this.loading = false;
            console.log(errorMsg);
            if (errorMsg.hasOwnProperty('non_field_errors')) {
                _this.showToast(errorMsg.non_field_errors);
            }
            else if (errorMsg.hasOwnProperty('detail')) {
                _this.showToast(errorMsg.detail);
            }
            else {
                var message = '';
                if (typeof errorMsg === 'string') {
                    message = errorMsg;
                }
                else {
                    for (var field in errorMsg) {
                        if (errorMsg.hasOwnProperty(field)) {
                            message = message + ' ' + field + ': ' + errorMsg[field];
                        }
                    }
                }
                _this.showToast(message);
            }
        });
    };
    HomePage.prototype.goToRegister = function () {
        this._nav.push(__WEBPACK_IMPORTED_MODULE_7__register_signup__["a" /* RegisterPage */]);
    };
    HomePage.prototype.goToForgotPassword = function () {
        // this._nav.push(ForgotPasswordPage);
    };
    HomePage.prototype.typed = function ($event) {
        if ($event.keyCode === 13) {
            this.login();
        }
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/login/home.html"*/'\n<ion-content padding>\n  <div class="dark-gradient"></div>\n    <!--<ion-title>Login</ion-title>-->\n\n    <form  novalidate class="padding-top">\n        <ion-row>\n            <ion-item>\n                <ion-label for="email"></ion-label>\n                <ion-input type="email" placeholder="username"  [(ngModel)]="user.username" name="username" (keypress)="typed($event)" ></ion-input>\n            </ion-item>\n        </ion-row>\n        <ion-row>\n            <ion-item>\n                <ion-label for="password"></ion-label>\n                <ion-input type="password" [(ngModel)]="user.password" name="password" (keypress)="typed($event)"  placeholder="Password" ></ion-input>\n            </ion-item>\n        </ion-row>\n    </form>\n\n      <div class="login-action-btns" padding-top>\n        <button  (tap)=goToRegister() class="register-button" color="light" ion-button block outline="">Sign up</button>\n\n        <button (tap)="login()" class="login-button" color="light" ion-button block outline="">Log in</button>\n      </div>\n\n    <!--new link to sign up page-->\n    <ion-row>\n        <button [navPush]="resetPasswordPage"  color="light" class="border" ion-button block outline>Reset password</button>\n    </ion-row>\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/login/home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__shared_services_session_service__["a" /* SessionService */],
        __WEBPACK_IMPORTED_MODULE_3__shared_services_auth_service__["a" /* AuthService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_6__shared_services_SettingsService__["a" /* SettingsService */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthTokenService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AuthTokenService = (function () {
    function AuthTokenService() {
        //
    }
    AuthTokenService.prototype.getToken = function () {
        var token = window.localStorage.getItem('auth-token');
        if (typeof token === 'undefined') {
            return null;
        }
        else {
            return token;
        }
    };
    AuthTokenService.prototype.setToken = function (token) {
        return window.localStorage.setItem('auth-token', token);
    };
    AuthTokenService.prototype.clearToken = function () {
        return window.localStorage.removeItem('auth-token');
    };
    return AuthTokenService;
}());
AuthTokenService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], AuthTokenService);

//# sourceMappingURL=authtoken.service.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__bases_services_BaseService__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__HttpSettingsService__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__session_service__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__authtoken_service__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_Auth__ = __webpack_require__(676);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_Observable__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AuthService = (function (_super) {
    __extends(AuthService, _super);
    function AuthService(http, _httpSettings, _sesstionService, _authToken) {
        var _this = _super.call(this, http, _httpSettings) || this;
        _this.http = http;
        _this._httpSettings = _httpSettings;
        _this._sesstionService = _sesstionService;
        _this._authToken = _authToken;
        _this._basePath = 'api-token-auth/';
        return _this;
    }
    AuthService.prototype.listMap = function (res) {
        var toReturn = res.json();
        for (var num in toReturn.results) {
            if (toReturn.results.hasOwnProperty(num)) {
                toReturn.results[num] = new __WEBPACK_IMPORTED_MODULE_6__models_Auth__["a" /* Auth */](toReturn.results[num]);
            }
        }
        return toReturn;
    };
    AuthService.prototype.singleMap = function (res) {
        return new __WEBPACK_IMPORTED_MODULE_6__models_Auth__["a" /* Auth */](res.json());
    };
    AuthService.prototype.login = function (data, params) {
        var _this = this;
        var options = {
            headers: this._httpSettings.getUnauthorizedHeaders(),
            search: new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* URLSearchParams */](this.makeStringOfParams(params))
        };
        return this.http.post(this.getUrl(), data, options)
            .map(function (res) {
            var toReturn = _this.singleMap(res);
            _this.singleObject = toReturn;
            _this.singleO.emit(toReturn);
            _this._authToken.setToken(toReturn.token);
            _this._sesstionService.actionLoggedIn();
            return toReturn;
        })
            .catch(this.handleError);
    };
    AuthService.prototype.logout = function () {
        var _this = this;
        var params = this._httpSettings.addTokenToParams({});
        var options = {
            headers: this._httpSettings.getHeaders(),
            search: new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* URLSearchParams */](this.makeStringOfParams(params))
        };
        return this.http.post(this._httpSettings.getBaseUrl() + 'logout/', '', options)
            .map(function (res) {
            var toReturn = res.json();
            return toReturn;
        }).do(function () {
            _this._authToken.clearToken();
            _this._sesstionService.logout();
        })
            .catch(this.handleError);
    };
    AuthService.prototype.handleError = function (error) {
        var json = error.json();
        // var toReturn = 'Server error';
        var toReturn = json;
        if (json.hasOwnProperty('error')) {
            toReturn = json.error;
        }
        if (json.hasOwnProperty('detail')) {
            toReturn = json.detail;
        }
        return __WEBPACK_IMPORTED_MODULE_7_rxjs_Observable__["Observable"].throw(toReturn);
    };
    return AuthService;
}(__WEBPACK_IMPORTED_MODULE_2__bases_services_BaseService__["a" /* BaseService */]));
AuthService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__HttpSettingsService__["a" /* HttpSettingsService */],
        __WEBPACK_IMPORTED_MODULE_4__session_service__["a" /* SessionService */],
        __WEBPACK_IMPORTED_MODULE_5__authtoken_service__["a" /* AuthTokenService */]])
], AuthService);

//# sourceMappingURL=auth.service.js.map

/***/ }),

/***/ 168:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ResetPasswordPage = (function () {
    function ResetPasswordPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    return ResetPasswordPage;
}());
ResetPasswordPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-reset-password',template:/*ion-inline-start:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/reset-password/reset-password.html"*/'<ion-header no-border="">\n  <ion-navbar transparent="">\n    <!--<ion-title>Ionic2Firebase</ion-title>-->\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div class="dark-gradient"></div>\n\n  <form [formGroup]="resetPasswordForm" (ngSubmit)="submit()" novalidate class="padding-top">\n    <ion-row>\n      <ion-item>\n        <ion-label for="email"></ion-label>\n        <ion-input type="email" value="" placeholder="Email" formControlName="email"></ion-input>\n    </ion-item>\n    </ion-row>\n    <br><br>\n    <ion-row>\n      <button ion-button class="login-button" color="light" type="submit" block outline>Reset password</button>\n    </ion-row>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/reset-password/reset-password.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
], ResetPasswordPage);

//# sourceMappingURL=reset-password.js.map

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListGymPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__view_gym_view_gym__ = __webpack_require__(98);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ListGymPage = (function () {
    function ListGymPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ListGymPage.prototype.goTogym = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__view_gym_view_gym__["a" /* GymPage */]);
    };
    return ListGymPage;
}());
ListGymPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-list-gym',template:/*ion-inline-start:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/list-gym/list-gym.html"*/'<ion-header no-border="">\n  <ion-navbar transparent>\n    <button ion-button menuToggle style="color: #fff">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title></ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding *ngIf="loading">\n  <div class="dark-gradient"></div>\n  <ion-list class="padding-top">\n    <ion-searchbar placeholder="Search " (ionInput)="getItems($event)"></ion-searchbar>\n  </ion-list>\n\n  <ion-list  *ngIf="gymResponse">\n  <ion-item *ngFor="let data of gymResponse">\n    <ion-thumbnail item-start>\n      <img [src]="data.image">\n      <!--<pre>{{data | json}}</pre>-->\n    </ion-thumbnail>\n    <h2>{{data.title}}</h2>\n    <p style="color: #f7f7f7" *ngFor="let location of data.location">\n      {{location.name}}</p>\n    <button ion-button clear item-end (tap)="goTogym(data)">View</button>\n  </ion-item>\n</ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/list-gym/list-gym.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
], ListGymPage);

//# sourceMappingURL=list-gym.js.map

/***/ }),

/***/ 187:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 187;

/***/ }),

/***/ 230:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 230;

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProfilePage = (function () {
    function ProfilePage() {
    }
    return ProfilePage;
}());
ProfilePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-profile',template:/*ion-inline-start:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/profile/profile.html"*/'<ion-header no-border="">\n  <ion-navbar transparent>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Edit Profile</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <div class="dark-gradient"></div>\n   <ion-grid fixed class="padding-top">\n        <ion-row>\n            <ion-col col-lg-6 push-lg-3 col-md-6 push-md-3 col-sm-12>\n              <!--<ion-title>Signup</ion-title>-->\n              <form [formGroup]="signupForm" (ngSubmit)="saveprofile()" novalidate>\n\n                    <ion-item>\n                        <ion-label for="firstname"></ion-label>\n                        <ion-input type="text" value="" placeholder="First Name" formControlName="firstname"></ion-input>\n                    </ion-item>\n\n                    <ion-item>\n                        <ion-label for="lastname"></ion-label>\n                        <ion-input type="text" value="" placeholder="Last Name" formControlName="lastname"></ion-input>\n                    </ion-item>\n\n                    <ion-item>\n                        <ion-label for="username"></ion-label>\n                        <ion-input type="text" value="" placeholder="Username" formControlName="username"></ion-input>\n                    </ion-item>\n\n                    <ion-item>\n                        <ion-label for="phonenumber"></ion-label>\n                        <ion-input type="text" value="" placeholder="Phone Number" formControlName="phonenumber"></ion-input>\n                    </ion-item>\n\n                <ion-item>\n                   <ion-label stacked>Gender:</ion-label>\n                   <ion-select\n                      formControlName="sex"\n                      >\n                       <ion-option value="Female">Female</ion-option>\n                        <ion-option value="Male">Male</ion-option>\n                         <ion-option value="Others">Others</ion-option>\n                   </ion-select>\n                </ion-item>\n\n                    <ion-item>\n                          <span\n                          ion-text\n                          color="primary"\n                          block\n                          text-center\n                          padding-top\n                          padding-bottom\n                          (click)="selectImage()">Select Profile Picture</span>\n                          <input\n                             type="hidden"\n                             name="image"\n                             formControlName="image"\n                             [(ngModel)]="pfImage">\n                          <img [src]="ProfileImage">\n                        </ion-item>\n\n                <br><br>\n\n                  <ion-row>\n                      <button class="register-button" ion-button type="submit" color="light" block outline>Sign up</button>\n                  </ion-row>\n              </form>\n            </ion-col>\n        </ion-row>\n   </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/profile/profile.html"*/
    }),
    __metadata("design:paramtypes", [])
], ProfilePage);

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaseService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_HttpSettingsService__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BaseService = (function () {
    function BaseService(http, _httpSettings) {
        this.http = http;
        this._httpSettings = _httpSettings;
        this._basePath = '';
        this.singleO = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.listO = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    BaseService.prototype.listMap = function (res) {
        return res.json();
    };
    BaseService.prototype.singleMap = function (res) {
        return res.json();
    };
    BaseService.prototype.getUrl = function (params) {
        if (typeof params === 'undefined') {
            return this._httpSettings.getBaseUrl() + this._basePath;
        }
        else {
            return this._httpSettings.getBaseUrl() + params;
        }
    };
    BaseService.prototype.getList = function (params) {
        var _this = this;
        params = this._httpSettings.addTokenToParams(params);
        var options = {
            headers: this._httpSettings.getHeaders(),
            search: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */](this.makeStringOfParams(params))
        };
        console.log(this.getUrl());
        return this.http.get(this.getUrl(), options)
            .map(function (res) {
            var toReturn = _this.listMap(res);
            _this.listObject = toReturn;
            _this.listO.emit(toReturn);
            return toReturn;
        })
            .catch(this.handleError);
    };
    BaseService.prototype.getNextList = function () {
        var _this = this;
        if (typeof this.listObject !== 'undefined') {
            console.log(this.listObject.next);
            if (this.listObject.next !== null) {
                var url = this.listObject.next;
                var options = {
                    headers: this._httpSettings.getHeaders()
                };
                return this.http.get(url, options)
                    .map(function (res) {
                    var toReturn = _this.listMap(res);
                    _this.listObject = toReturn;
                    _this.listO.emit(toReturn);
                    return toReturn;
                })
                    .catch(this.handleError);
            }
        }
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].from([]);
    };
    BaseService.prototype.get = function (id, params) {
        var _this = this;
        params = this._httpSettings.addTokenToParams(params);
        var options = {
            headers: this._httpSettings.getHeaders(),
            search: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */](this.makeStringOfParams(params))
        };
        return this.http.get(this.getUrl() + id + '/', options)
            .map(function (res) {
            var toReturn = _this.singleMap(res);
            _this.singleObject = toReturn;
            _this.singleO.emit(toReturn);
            return toReturn;
        })
            .catch(this.handleError);
    };
    // new
    BaseService.prototype.post = function (data, params) {
        var _this = this;
        params = this._httpSettings.addTokenToParams(params);
        var options = {
            headers: this._httpSettings.getHeaders(),
            search: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */](this.makeStringOfParams(params))
        };
        return this.http.post(this.getUrl(), data, options)
            .map(function (res) {
            var toReturn = _this.singleMap(res);
            _this.singleObject = toReturn;
            _this.singleO.emit(toReturn);
            return toReturn;
        })
            .catch(this.handleError);
    };
    // update
    BaseService.prototype.put = function (id, data, params) {
        var _this = this;
        params = this._httpSettings.addTokenToParams(params);
        var options = {
            headers: this._httpSettings.getHeaders(),
            search: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */](this.makeStringOfParams(params))
        };
        return this.http.put(this.getUrl() + id + '/', data, options)
            .map(function (res) {
            var toReturn = _this.singleMap(res);
            _this.singleObject = toReturn;
            _this.singleO.emit(toReturn);
            return toReturn;
        })
            .catch(this.handleError);
    };
    // partial update???
    BaseService.prototype.patch = function (id, data, params) {
        var _this = this;
        params = this._httpSettings.addTokenToParams(params);
        var options = {
            headers: this._httpSettings.getHeaders(),
            search: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */](this.makeStringOfParams(params))
        };
        return this.http.patch(this.getUrl() + id + '/', data, options)
            .map(function (res) {
            var toReturn = _this.singleMap(res);
            _this.singleObject = toReturn;
            _this.singleO.emit(toReturn);
            return toReturn;
        })
            .catch(this.handleError);
    };
    BaseService.prototype.delete = function (id, params) {
        params = this._httpSettings.addTokenToParams(params);
        var options = {
            headers: this._httpSettings.getHeaders(),
            search: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* URLSearchParams */](this.makeStringOfParams(params))
        };
        return this.http.delete(this.getUrl() + id + '/', options)
            .map(function (res) {
            return res;
        })
            .catch(this.handleError);
    };
    BaseService.prototype.handleError = function (error) {
        var json = {
            error: "unknown",
            detail: "unknown"
        };
        try {
            json = error.json();
        }
        catch (e) {
            console.log(e);
        }
        // var toReturn = 'Server error';
        var toReturn = json;
        if (json.hasOwnProperty('error')) {
            toReturn = json.error;
        }
        if (json.hasOwnProperty('detail')) {
            toReturn = json.detail;
        }
        console.error('error', JSON.stringify(toReturn));
        if (typeof toReturn.match !== 'undefined') {
            if (toReturn.match(/invalid token/i)) {
                window.localStorage.removeItem('auth-token');
                this._httpSettings.notLoggedIn();
            }
        }
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(toReturn);
    };
    BaseService.prototype.makeStringOfParams = function (obj) {
        var toReturn = '';
        var qsArray = [];
        for (var field in obj) {
            if (obj.hasOwnProperty(field)) {
                if (obj[field] === true) {
                    obj[field] = 'True';
                }
                if (obj[field] === false) {
                    obj[field] = 'False';
                }
                qsArray.push(field + '=' + obj[field]);
            }
        }
        toReturn = qsArray.join('&');
        return toReturn;
    };
    BaseService.prototype.removeEmptyFields = function (obj) {
        var newObj = {};
        for (var field in obj) {
            if (obj.hasOwnProperty(field)) {
                if ((obj[field] === true || obj[field] === false) ||
                    (obj[field] !== '' && obj[field] !== null)) {
                    newObj[field] = obj[field];
                }
            }
        }
        return newObj;
    };
    BaseService.prototype.mergeLists = function (listFrom, listTo) {
        for (var index in listFrom) {
            if (listFrom.hasOwnProperty(index)) {
                for (var listToIndex in listTo) {
                    if (listTo.hasOwnProperty(listToIndex)) {
                        if (listTo[listToIndex].id === listFrom[index].id) {
                            listTo[listToIndex] = listFrom[index];
                        }
                    }
                }
            }
        }
        return listTo;
    };
    return BaseService;
}());
BaseService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__services_HttpSettingsService__["a" /* HttpSettingsService */]])
], BaseService);

//# sourceMappingURL=BaseService.js.map

/***/ }),

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_models_User__ = __webpack_require__(678);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_services_register_service__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_home__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_services_session_service__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_services_auth_service__ = __webpack_require__(160);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var RegisterPage = (function () {
    function RegisterPage(_nav, _sessionService, _authService, _registerService, loadingCtrl, toastCtrl, alertCtrl) {
        this._nav = _nav;
        this._sessionService = _sessionService;
        this._authService = _authService;
        this._registerService = _registerService;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.user = new __WEBPACK_IMPORTED_MODULE_2__shared_models_User__["a" /* UserRegister */]({});
        this.loading = false;
    }
    RegisterPage.prototype.ionViewDidEnter = function () {
    };
    RegisterPage.prototype.showLoading = function () {
        this.loadingController = this.loadingCtrl.create({
            content: "Signing Up"
        });
        this.loadingController.present();
    };
    RegisterPage.prototype.hideLoading = function () {
        this.loadingController.dismiss();
    };
    RegisterPage.prototype.register = function () {
        var _this = this;
        var tester = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-?\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
        var valid = false;
        if (this.user.email.length > 254 || this.user.email.length < 5) {
            valid = false;
        }
        else {
            valid = tester.test(this.user.email);
        }
        if (!valid) {
            this.toastCtrl.create({
                message: 'Please enter a valid email address.',
                duration: 3000,
                position: 'middle'
            }).present();
        }
        else {
            this.showLoading();
            this.loading = true;
            this._registerService.post(JSON.stringify(this.user))
                .subscribe(function (res) {
                _this.alertCtrl.create({
                    title: 'Thanks for Registering!',
                    message: "Welcome to Car Hire",
                    buttons: [
                        {
                            text: 'OK',
                            handler: function () {
                                _this._nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__login_home__["a" /* HomePage */]);
                            }
                        }
                    ]
                }).present();
                _this.hideLoading();
                _this.loading = false;
            }, function (errorMsg) {
                _this.loading = false;
                _this.hideLoading();
                var text = '';
                for (var field in errorMsg) {
                    if (errorMsg.hasOwnProperty(field)) {
                        if (field === 'username') {
                            if (errorMsg[field] === 'Username already exists.') {
                                text = text + "The provided email address has already been used to register.\n";
                            }
                            else {
                                text = text + field + ': ' + errorMsg[field] + "\n";
                            }
                        }
                        else {
                            text = text + field + ': ' + errorMsg[field] + "\n";
                        }
                    }
                }
                if (errorMsg.hasOwnProperty('non_field_errors')) {
                    text = text + errorMsg.non_field_errors;
                }
                _this.toastCtrl.create({
                    message: text,
                    duration: 5000,
                    position: 'middle'
                }).present();
            });
        }
    };
    RegisterPage.prototype.goToLogin = function () {
        this._nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__login_home__["a" /* HomePage */]);
    };
    return RegisterPage;
}());
RegisterPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-signup',template:/*ion-inline-start:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/register/signup.html"*/'<ion-header no-border="">\n  <ion-navbar color="primary" transparent>\n    <ion-title>Register</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div class="dark-gradient"></div>\n   <ion-grid fixed style="padding-top: 90px;">\n        <ion-row>\n            <ion-col col-lg-6 push-lg-3 col-md-6 push-md-3 col-sm-12>\n              <!--<ion-title>Signup</ion-title>-->\n              <form novalidate submit="return false;">\n\n                  <ion-row>\n                    <ion-item>\n                        <ion-label for="username"></ion-label>\n                        <ion-input type="text" [(ngModel)]="user.username" name="username" placeholder="UserName" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n\n                  <ion-row>\n                    <ion-item>\n                        <ion-label for="FirstName"></ion-label>\n                        <ion-input type="text" [(ngModel)]="user.first_name" name="first_name" placeholder="First Name" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n\n\n                  <ion-row>\n                    <ion-item>\n                        <ion-label for="LastName"></ion-label>\n                        <ion-input type="text"  [(ngModel)]="user.last_name" name="last_name" placeholder="Last Name" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n                  <ion-row>\n                    <ion-item>\n                        <ion-label for="PhoneNumber"></ion-label>\n                        <ion-input type="text" [(ngModel)]="user.mobile_number" name="mobile_number" placeholder="Mobile  Number" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n\n\n                  <ion-row>\n                    <ion-item>\n                        <ion-label for="email"></ion-label>\n                        <ion-input type="email"  [(ngModel)]="user.email" name="email" placeholder="Email" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n                  <ion-row>\n                    <ion-item>\n                        <ion-label for="password"></ion-label>\n                        <ion-input type="password" [(ngModel)]="user.password" name="password" placeholder="Password" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n                <br><br>\n\n                  <ion-row>\n                      <button (tap)="register()" class="register-button" ion-button type="submit" color="light" block outline>Sign up</button>\n                  </ion-row>\n              </form>\n            </ion-col>\n        </ion-row>\n   </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/register/signup.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_5__shared_services_session_service__["a" /* SessionService */],
        __WEBPACK_IMPORTED_MODULE_6__shared_services_auth_service__["a" /* AuthService */],
        __WEBPACK_IMPORTED_MODULE_3__shared_services_register_service__["a" /* RegisterService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], RegisterPage);

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_bases_services_BaseService__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_services_HttpSettingsService__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__session_service__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_RegisterResponse__ = __webpack_require__(679);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var RegisterService = (function (_super) {
    __extends(RegisterService, _super);
    function RegisterService(http, _httpSettings, _sesstionService) {
        var _this = _super.call(this, http, _httpSettings) || this;
        _this.http = http;
        _this._httpSettings = _httpSettings;
        _this._sesstionService = _sesstionService;
        _this._basePath = 'auth-register/';
        return _this;
    }
    RegisterService.prototype.listMap = function (res) {
        var toReturn = res.json();
        for (var num in toReturn.results) {
            if (toReturn.results.hasOwnProperty(num)) {
                toReturn.results[num] = new __WEBPACK_IMPORTED_MODULE_6__models_RegisterResponse__["a" /* RegisterResponse */](toReturn.results[num]);
            }
        }
        return toReturn;
    };
    RegisterService.prototype.singleMap = function (res) {
        return new __WEBPACK_IMPORTED_MODULE_6__models_RegisterResponse__["a" /* RegisterResponse */](res.json());
    };
    RegisterService.prototype.handleError = function (error) {
        console.error(error);
        var json = error.json();
        // var toReturn = 'Server error';
        var toReturn = json;
        if (json.hasOwnProperty('error')) {
            toReturn = json.error;
        }
        if (json.hasOwnProperty('detail')) {
            toReturn = json.detail;
        }
        return __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__["Observable"].throw(toReturn);
    };
    return RegisterService;
}(__WEBPACK_IMPORTED_MODULE_1__shared_bases_services_BaseService__["a" /* BaseService */]));
RegisterService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__shared_services_HttpSettingsService__["a" /* HttpSettingsService */],
        __WEBPACK_IMPORTED_MODULE_5__session_service__["a" /* SessionService */]])
], RegisterService);

//# sourceMappingURL=register.service.js.map

/***/ }),

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__view_gym_view_gym__ = __webpack_require__(98);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardPage = (function () {
    function DashboardPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DashboardPage.prototype.goTogym = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__view_gym_view_gym__["a" /* GymPage */]);
    };
    return DashboardPage;
}());
DashboardPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-dashboard',template:/*ion-inline-start:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/dashboard/dashboard.html"*/'<ion-header no-border="">\n  <ion-navbar transparent>\n    <button ion-button menuToggle style="color: #fff">\n      <ion-icon name="menu"></ion-icon>\n        <ion-title>Admin Dashboard</ion-title>\n    </button>\n    <ion-title></ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding >\n  <div class="dark-gradient"></div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/dashboard/dashboard.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
], DashboardPage);

//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 348:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(349);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(353);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 353:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export firebaseConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_home__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angularfire2_auth__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_products_products__ = __webpack_require__(680);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_reset_password_reset_password__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_register_signup__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_add_gym_add_gym__ = __webpack_require__(681);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_angularfire2_database__ = __webpack_require__(682);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_list_gym_list_gym__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_view_gym_view_gym__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__angular_http__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_fitness_centers_fitness_centers__ = __webpack_require__(723);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_wellness_centers_wellness_center__ = __webpack_require__(724);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_profile_profile__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__shared_services_session_service__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__shared_services_auth_service__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__shared_services_register_service__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__shared_services_authtoken_service__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__shared_services_SettingsService__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__shared_services_HttpSettingsService__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_dashboard_dashboard__ = __webpack_require__(315);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



























var firebaseConfig = {
    apiKey: "AIzaSyC6A-d2VJwK164tyV-YiA6vRBFb1xUkt7s",
    authDomain: "fir-project-e5251.firebaseapp.com",
    databaseURL: "https://fir-project-e5251.firebaseio.com",
    projectId: "fir-project-e5251",
    storageBucket: "gs://fir-project-e5251.appspot.com/",
    messagingSenderId: "902921514274"
};
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_login_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_products_products__["a" /* ProductsPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_reset_password_reset_password__["a" /* ResetPasswordPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_register_signup__["a" /* RegisterPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_add_gym_add_gym__["a" /* AddGymPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_list_gym_list_gym__["a" /* ListGymPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_view_gym_view_gym__["a" /* GymPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_fitness_centers_fitness_centers__["a" /* FitnessCentersPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_wellness_centers_wellness_center__["a" /* WellnessCenterPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_dashboard_dashboard__["a" /* DashboardPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_16__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                links: []
            }),
            __WEBPACK_IMPORTED_MODULE_7_angularfire2__["a" /* AngularFireModule */].initializeApp(firebaseConfig),
            __WEBPACK_IMPORTED_MODULE_8_angularfire2_auth__["b" /* AngularFireAuthModule */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_login_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_products_products__["a" /* ProductsPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_reset_password_reset_password__["a" /* ResetPasswordPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_register_signup__["a" /* RegisterPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_add_gym_add_gym__["a" /* AddGymPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_list_gym_list_gym__["a" /* ListGymPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_view_gym_view_gym__["a" /* GymPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_fitness_centers_fitness_centers__["a" /* FitnessCentersPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_wellness_centers_wellness_center__["a" /* WellnessCenterPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_dashboard_dashboard__["a" /* DashboardPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_13_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_20__shared_services_session_service__["a" /* SessionService */],
            __WEBPACK_IMPORTED_MODULE_21__shared_services_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_22__shared_services_register_service__["a" /* RegisterService */],
            __WEBPACK_IMPORTED_MODULE_23__shared_services_authtoken_service__["a" /* AuthTokenService */],
            __WEBPACK_IMPORTED_MODULE_24__shared_services_SettingsService__["a" /* SettingsService */],
            __WEBPACK_IMPORTED_MODULE_25__shared_services_HttpSettingsService__["a" /* HttpSettingsService */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_profile_profile__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_home__ = __webpack_require__(158);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, auth) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.auth = auth;
        //TODO: return login page
        this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_login_home__["a" /* HomePage */];
        this.currentUser = [];
        this.rawList = [];
        this.userResponse = [];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [];
        this.settings = [];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.editProfile = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_profile_profile__["a" /* ProfilePage */]);
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/app/app.html"*/'\n\n\n<ion-menu [content]="content" >\n\n  <ion-content #menu class="menu" color="dark" class="menu-bg" >\n\n    <ion-card text-center class="hide-card card-color"  >\n\n      <br>\n      <img src="assets/imgs/rk.jpg" class="custom-avatar" />\n      <br>\n      <h2 style="color: white">Stella Sikhila</h2>\n      <p style="color: white">Nairobi, Kenya</p>\n    <hr>\n  </ion-card>\n\n    <ion-item-group color="dark" class="menu-bg" >\n      <!--<ion-item-divider class="card-color">Users</ion-item-divider>-->\n      <button  menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)" class="menu-bg" >\n        {{p.title}}\n      </button>\n    </ion-item-group>\n\n    <!--<ion-item-group color="dark" >-->\n        <!--<ion-item-divider class="card-color">Admin</ion-item-divider>-->\n        <!--<button class="menu-bg" color="dark" menuClose ion-item *ngFor="let p of admin" (click)="openPage(p)">-->\n          <!--&lt;!&ndash;<ion-icon [name]="p.icon" item-left></ion-icon>&ndash;&gt;-->\n          <!--{{p.title}}-->\n        <!--</button>-->\n      <!--</ion-item-group>-->\n\n    <ion-item-group color="dark">\n        <ion-item-divider class="card-color">Settings</ion-item-divider>\n        <button class="menu-bg"  color="dark" menuClose ion-item >\n          <!--<ion-icon [name]="p.icon" item-left></ion-icon>-->\n          Log Out\n        </button>\n      </ion-item-group>\n\n\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n<!--ionic cordova run android --device-->\n'/*ion-inline-end:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
        __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["a" /* AngularFireAuth */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 427:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ENV; });
var ENV = (function () {
    function ENV() {
    }
    return ENV;
}());

ENV.DEV_MODE = false;
ENV.DESKTOP_MODE = false;
// public static PROTOCOL = 'https';
// public static DOMAIN = 'platform.tunaweza.com';
ENV.BASE_URL = '/api/';
ENV.API_VERSION = '1';
ENV.PROTOCOL = 'http';
ENV.DOMAIN = '127.0.0.1:8000';
ENV.APP_VERSION = '0.1.5';
ENV.PUSH_GROUP = 'tw_prod'; // make tw_dev for dev builds
ENV.PUSH_GOOGLE_SENDER_ID = '502801541439';
ENV.UPLOAD_BASE = 'https://platform.tunaweza.com/static/uploads/';
ENV.ENABLE_ANALYTICS = true;
ENV.ANALYTICS = {
    GOOGLE: {
        TRACKING_ID: 'UA-75702652-299'
    }
};
//# sourceMappingURL=env.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SessionService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SettingsService__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__authtoken_service__ = __webpack_require__(159);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SessionService = (function () {
    function SessionService(_authToken, _http, _settings) {
        this._authToken = _authToken;
        this._http = _http;
        this._settings = _settings;
        this.authStatus = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.userObservable = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this._basePath = 'api-token-auth/';
        this._apiVersion = '1';
    }
    SessionService.prototype.getToken = function () {
        return this._authToken.getToken();
    };
    SessionService.prototype.logout = function () {
        var toReturn = this._authToken.clearToken();
        this.actionLoggedOut();
        return toReturn;
    };
    SessionService.prototype.actionLoggedIn = function () {
        this.authStatus.emit({
            'authenticated': this.isLoggedIn()
        });
    };
    SessionService.prototype.actionLoggedOut = function () {
        this.authStatus.emit({
            'authenticated': this.isLoggedIn()
        });
        this.setUser(undefined);
    };
    SessionService.prototype.isLoggedIn = function () {
        if (this.getToken() != null) {
            return true;
        }
        else {
            return false;
        }
    };
    SessionService.prototype.setUser = function (user) {
        this.user = user;
        if (user !== null) {
            this.userObservable.emit(user);
        }
    };
    SessionService.prototype.notLoggedIn = function () {
        // this._nav.setRoot(LoginPage);
    };
    return SessionService;
}());
SessionService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__authtoken_service__["a" /* AuthTokenService */],
        __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__SettingsService__["a" /* SettingsService */]])
], SessionService);

//# sourceMappingURL=session.service.js.map

/***/ }),

/***/ 676:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Auth; });
var Auth = (function () {
    function Auth(obj) {
        for (var field in obj) {
            if (obj.hasOwnProperty(field)) {
                this[field] = obj[field];
            }
        }
    }
    return Auth;
}());

//# sourceMappingURL=Auth.js.map

/***/ }),

/***/ 677:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthUser; });
var AuthUser = (function () {
    function AuthUser(obj) {
        for (var field in obj) {
            if (obj.hasOwnProperty(field)) {
                this[field] = obj[field];
            }
        }
    }
    return AuthUser;
}());

//# sourceMappingURL=AuthUser.js.map

/***/ }),

/***/ 678:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export UserProfile */
/* unused harmony export User */
/* unused harmony export UserMinimal */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserRegister; });
var UserProfile = (function () {
    function UserProfile(obj) {
        this.is_approver = false;
        this.is_admin = false;
        this.is_staff = false;
        for (var field in obj) {
            if (obj.hasOwnProperty(field)) {
                this[field] = obj[field];
            }
        }
    }
    return UserProfile;
}());

var User = (function () {
    function User(obj) {
        this.first_name = '';
        this.last_name = '';
        this.is_staff = false;
        this.is_superuser = false;
        this.email = '';
        this.phone_number = '';
        this.gender = '';
        this.userprofile = new UserProfile({});
        this.is_admin = false;
        for (var field in obj) {
            if (obj.hasOwnProperty(field)) {
                if (field === 'pk') {
                    this[field] = obj[field];
                    this.id = obj[field];
                }
                else {
                    this[field] = obj[field];
                }
            }
        }
    }
    User.prototype.getName = function () {
        if (typeof this.first_name === 'undefined' ||
            typeof this.last_name === 'undefined' ||
            typeof this.first_name === null ||
            typeof this.last_name === null) {
            console.error('Please set the user\'s name for user id: ' + this.id);
            return this.username;
        }
        else {
            return this.first_name + ' ' + this.last_name;
        }
    };
    return User;
}());

var UserMinimal = (function () {
    function UserMinimal(obj) {
        this.full_name = '';
        this.first_name = '';
        this.last_name = '';
        for (var field in obj) {
            if (obj.hasOwnProperty(field)) {
                if (field === 'pk') {
                    this[field] = obj[field];
                    this.id = obj[field];
                }
                else {
                    this[field] = obj[field];
                }
            }
        }
    }
    UserMinimal.prototype.getName = function () {
        if (typeof this.first_name === 'undefined' ||
            typeof this.last_name === 'undefined' ||
            typeof this.first_name === null ||
            typeof this.last_name === null) {
            console.error('Please set the user\'s name for user id: ' + this.id);
            return this.id;
        }
        else {
            return this.first_name + ' ' + this.last_name;
        }
    };
    return UserMinimal;
}());

var UserRegister = (function () {
    function UserRegister(obj) {
        this.email = '';
        this.password = '';
        this.password_again = '';
        this.first_name = '';
        this.last_name = '';
        this.mobile_number = '';
        this.username = '';
        this.invitation_code = '';
        for (var field in obj) {
            if (obj.hasOwnProperty(field)) {
                this[field] = obj[field];
            }
        }
    }
    return UserRegister;
}());

//# sourceMappingURL=User.js.map

/***/ }),

/***/ 679:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterResponse; });
var RegisterResponse = (function () {
    function RegisterResponse(obj) {
        this.will_account_be_activated = false;
        for (var field in obj) {
            if (obj.hasOwnProperty(field)) {
                this[field] = obj[field];
            }
        }
    }
    return RegisterResponse;
}());

//# sourceMappingURL=RegisterResponse.js.map

/***/ }),

/***/ 680:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__reset_password_reset_password__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__view_gym_view_gym__ = __webpack_require__(98);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProductsPage = (function () {
    function ProductsPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.resetPasswordPage = __WEBPACK_IMPORTED_MODULE_2__reset_password_reset_password__["a" /* ResetPasswordPage */]; //Added reset password page
    }
    ProductsPage.prototype.goTogym = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__view_gym_view_gym__["a" /* GymPage */]);
    };
    return ProductsPage;
}());
ProductsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-products',template:/*ion-inline-start:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/products/products.html"*/'<ion-header no-border="">\n  <ion-navbar transparent>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding >\n    <div class="dark-gradient"></div>\n<ion-grid style="margin-top: 50px">\n\n  <ion-list>\n    <ion-searchbar placeholder="Search " ></ion-searchbar>\n  </ion-list>\n\n    <ion-list>\n    <ion-item >\n      <ion-thumbnail item-start>\n        <img src="assets/images/1.jpg">\n        <!--<pre>{{data | json}}</pre>-->\n      </ion-thumbnail>\n      <h2>Prado</h2>\n      <h3>Price per Day: ksh 3000</h3>\n      <p style="color: #f7f7f7">\n        Location: Thika</p>\n      <button ion-button clear item-end (tap)="goTogym()">View</button>\n      <button ion-button clear item-end (tap)="edit()">Edit</button>\n    </ion-item>\n  </ion-list>\n\n   <ion-list>\n    <ion-item >\n      <ion-thumbnail item-start>\n        <img src="assets/images/5.jpg">\n        <!--<pre>{{data | json}}</pre>-->\n      </ion-thumbnail>\n      <h2>Prado</h2>\n      <h3>Price per Day: ksh 3000</h3>\n      <p style="color: #f7f7f7">\n        Location: Thika</p>\n      <button ion-button clear item-end (tap)="goTogym()">View</button>\n      <button ion-button clear item-end (tap)="edit()">Edit</button>\n    </ion-item>\n  </ion-list>\n\n   <ion-list>\n    <ion-item >\n      <ion-thumbnail item-start>\n        <img src="assets/images/4.jpg">\n        <!--<pre>{{data | json}}</pre>-->\n      </ion-thumbnail>\n      <h2>Prado</h2>\n      <h3>Price per Day: ksh 3000</h3>\n      <p style="color: #f7f7f7">\n        Location: Thika</p>\n      <button ion-button clear item-end (tap)="goTogym()">View</button>\n      <button ion-button clear item-end (tap)="edit()">Edit</button>\n    </ion-item>\n  </ion-list>\n\n   <ion-list>\n    <ion-item >\n      <ion-thumbnail item-start>\n        <img src="assets/images/2.jpg">\n        <!--<pre>{{data | json}}</pre>-->\n      </ion-thumbnail>\n      <h2>Prado</h2>\n      <h3>Price per Day: ksh 3000</h3>\n      <p style="color: #f7f7f7">\n        Location: Thika</p>\n      <button ion-button clear item-end (tap)="goTogym()">View</button>\n      <button ion-button clear item-end (tap)="edit()">Edit</button>\n    </ion-item>\n  </ion-list>\n\n   <ion-list>\n    <ion-item >\n      <ion-thumbnail item-start>\n        <img src="assets/images/3.jpg">\n        <!--<pre>{{data | json}}</pre>-->\n      </ion-thumbnail>\n      <h2>Prado</h2>\n      <h3>Price per Day: ksh 3000</h3>\n      <p style="color: #f7f7f7">\n        Location: Thika</p>\n      <button ion-button clear item-end (tap)="goTogym()">View</button>\n      <button ion-button clear item-end (tap)="edit()">Edit</button>\n    </ion-item>\n  </ion-list>\n\n\n\n\n\n\n  </ion-grid>\n\n\n\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/products/products.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]])
], ProductsPage);

//# sourceMappingURL=products.js.map

/***/ }),

/***/ 681:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddGymPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AddGymPage = (function () {
    function AddGymPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    return AddGymPage;
}());
AddGymPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-add-gym',template:/*ion-inline-start:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/add-gym/add-gym.html"*/'<ion-header no-border="">\n  <ion-navbar transparent>\n    <button ion-button menuToggle style="color: #fff">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Add Gym</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div class="dark-gradient"></div>\n   <ion-grid fixed class="padding-top">\n        <ion-row>\n            <ion-col col-lg-6 push-lg-3 col-md-6 push-md-3 col-sm-12>\n              <!--<ion-title>Signup</ion-title>-->\n              <form novalidate>\n                  <ion-row>\n                    <ion-item>\n                        <ion-label for="name"></ion-label>\n                        <ion-input [(ngModel)]="name" type="text" name="name" value="" placeholder="Gym Name" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n\n                <ion-row>\n                    <ion-item>\n                        <ion-label for="location"></ion-label>\n                        <ion-input [(ngModel)]="location" name="location" type="text" value="" placeholder="location" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n\n                <ion-row>\n                    <ion-item>\n                        <ion-label for="phone"></ion-label>\n                        <ion-input [(ngModel)]="phone" type="text" name="phone" value="" placeholder="Phone Number" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n\n                <ion-row>\n                    <ion-item>\n                        <ion-label for="email"></ion-label>\n                        <ion-input [(ngModel)]="email" type="email" name="email" value="" placeholder="email" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n\n                <ion-row>\n                    <ion-item>\n                        <ion-label for="description"></ion-label>\n                        <ion-input [(ngModel)]="description" type="text" name="description" value="" placeholder="description" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n\n\n                <br><br>\n\n                  <ion-row>\n                      <button  (click)="addGym(name,location,phone,email,description)" class="register-button" ion-button type="submit" color="light" block outline>Add Gym</button>\n                  </ion-row>\n              </form>\n            </ion-col>\n        </ion-row>\n   </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/add-gym/add-gym.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
], AddGymPage);

//# sourceMappingURL=add-gym.js.map

/***/ }),

/***/ 723:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FitnessCentersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__reset_password_reset_password__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__list_gym_list_gym__ = __webpack_require__(176);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




 //Added reset password page
var FitnessCentersPage = (function () {
    function FitnessCentersPage(navCtrl, fb) {
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.resetPasswordPage = __WEBPACK_IMPORTED_MODULE_3__reset_password_reset_password__["a" /* ResetPasswordPage */]; //Added reset password page
    }
    FitnessCentersPage.prototype.goToGym = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__list_gym_list_gym__["a" /* ListGymPage */]);
    };
    return FitnessCentersPage;
}());
FitnessCentersPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'fitness-centers',template:/*ion-inline-start:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/fitness-centers/fitness-centers.html"*/'\n<ion-header no-border="">\n  <ion-navbar transparent>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Fitness Centers</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding >\n    <div class="dark-gradient"></div>\n<ion-grid style="margin-top: 100px">\n    <ion-row >\n      <ion-col col-lg-12 push-lg-0 col-md-12 push-md-0 no-padding>\n        <div>\n          <ion-row no-padding class="discovery-row">\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h1>Aerobic Centers</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h1>Yoga Centers</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h1>Dance Centers</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h1>Pilates Centers</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h1>Gyms Centers</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h1>Athletic Clubs</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <!--<ion-card style="margin-top: 100px" (click)="goToGym()">-->\n    <!--<img src="assets/imgs/14.jpg" class="image"/>-->\n    <!--<div class="card-subtitle">Inspirational content</div>-->\n    <!--<div class="card-title"> Aerobic Centers</div>-->\n  <!--</ion-card>-->\n\n  <!--<ion-card>-->\n    <!--<img src="assets/imgs/20.jpg" class="image"/>-->\n    <!--<<div class="card-subtitle">Inspirational content</div>-->\n    <!--<div class="card-title">Yoga Centers</div>-->\n  <!--</ion-card>-->\n\n  <!--<ion-card>-->\n    <!--<img src="assets/imgs/13.jpg" class="image"/>-->\n    <!--<div class="card-subtitle">Inspirational content</div>-->\n    <!--<div class="card-title">Dance Centers</div>-->\n  <!--</ion-card>-->\n\n   <!--<ion-card (click)="goToGym()">-->\n    <!--<img src="assets/imgs/14.jpg" class="image"/>-->\n    <!--<div class="card-subtitle">Inspirational content</div>-->\n    <!--<div class="card-title"> Pilates Centers</div>-->\n  <!--</ion-card>-->\n\n  <!--<ion-card>-->\n    <!--<img src="assets/imgs/20.jpg" class="image"/>-->\n    <!--<<div class="card-subtitle">Inspirational content</div>-->\n    <!--<div class="card-title">Gyms</div>-->\n  <!--</ion-card>-->\n\n  <!--<ion-card  (click)="goToGym()">-->\n    <!--<img src="assets/imgs/14.jpg" class="image"/>-->\n    <!--<div class="card-subtitle">Inspirational content</div>-->\n    <!--<div class="card-title"> Country Clubs</div>-->\n  <!--</ion-card>-->\n\n  <!--<ion-card>-->\n    <!--<img src="assets/imgs/20.jpg" class="image"/>-->\n    <!--<<div class="card-subtitle">Inspirational content</div>-->\n    <!--<div class="card-title">Athletic Clubs</div>-->\n  <!--</ion-card>-->\n\n\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/fitness-centers/fitness-centers.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */]])
], FitnessCentersPage);

//# sourceMappingURL=fitness-centers.js.map

/***/ }),

/***/ 724:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WellnessCenterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__list_gym_list_gym__ = __webpack_require__(176);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var WellnessCenterPage = (function () {
    function WellnessCenterPage(navCtrl, fb) {
        this.navCtrl = navCtrl;
        this.fb = fb;
    }
    WellnessCenterPage.prototype.goToGym = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__list_gym_list_gym__["a" /* ListGymPage */]);
    };
    return WellnessCenterPage;
}());
WellnessCenterPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'wellness-center',template:/*ion-inline-start:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/wellness-centers/wellness-center.html"*/'<script src="fitness-centers.ts"></script>\n<ion-header no-border="">\n  <ion-navbar transparent>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Wellness Centers</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding >\n    <div class="dark-gradient"></div>\n<ion-grid style="margin-top: 100px">\n    <ion-row >\n      <ion-col col-lg-12 push-lg-0 col-md-12 push-md-0 no-padding>\n        <div>\n          <ion-row no-padding class="discovery-row">\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h3>Centers for General Health and Well-Being </h3>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h3>Centers that offer Specific Health Services</h3>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n\n\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h3>Well-Being Centers Run By Physicians</h3>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n          </ion-row>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/wellness-centers/wellness-center.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */]])
], WellnessCenterPage);

//# sourceMappingURL=wellness-center.js.map

/***/ }),

/***/ 90:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__constant_env__ = __webpack_require__(427);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SettingsService = (function () {
    function SettingsService() {
        this.devMode = __WEBPACK_IMPORTED_MODULE_1__constant_env__["a" /* ENV */].DEV_MODE;
        this.protocol = __WEBPACK_IMPORTED_MODULE_1__constant_env__["a" /* ENV */].PROTOCOL;
        this.domain = __WEBPACK_IMPORTED_MODULE_1__constant_env__["a" /* ENV */].DOMAIN;
        this.baseUrl = __WEBPACK_IMPORTED_MODULE_1__constant_env__["a" /* ENV */].BASE_URL;
        this.apiVersion = __WEBPACK_IMPORTED_MODULE_1__constant_env__["a" /* ENV */].API_VERSION;
        this.uploadBase = __WEBPACK_IMPORTED_MODULE_1__constant_env__["a" /* ENV */].UPLOAD_BASE;
        this.defaultUserSettings = {
            savePictureToCameraRoll: true,
            olderDeviceMode: 'not set',
        };
        // if (!this.devMode) {
        //     this.protocol = 'http';
        //     this.domain = '127.0.0.1';
        // }
    }
    SettingsService.prototype.getUploadBase = function () {
        return this.uploadBase;
    };
    SettingsService.prototype.getProtocol = function () {
        return this.protocol;
    };
    SettingsService.prototype.isDevMode = function () {
        return this.devMode;
    };
    SettingsService.prototype.getDomain = function () {
        return this.domain;
    };
    SettingsService.prototype.getBaseUrl = function (version) {
        if (typeof version === 'undefined') {
            version = this.apiVersion;
        }
        return this.baseUrl + 'v' + version + '/';
    };
    SettingsService.prototype.getUserSetting = function (key) {
        var r = JSON.parse(window.localStorage.getItem(key));
        if (r === null) {
            r = this.defaultUserSettings[key];
        }
        return r;
    };
    SettingsService.prototype.setUserSetting = function (key, value) {
        return window.localStorage.setItem(key, JSON.stringify(value));
    };
    return SettingsService;
}());
SettingsService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], SettingsService);

//# sourceMappingURL=SettingsService.js.map

/***/ }),

/***/ 91:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpSettingsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__session_service__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__SettingsService__ = __webpack_require__(90);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HttpSettingsService = (function () {
    function HttpSettingsService(_sessionService, _settings) {
        this._sessionService = _sessionService;
        this._settings = _settings;
        this.protocol = _settings.getProtocol();
        this.domain = _settings.getDomain();
        this.baseUrl = _settings.getBaseUrl();
    }
    HttpSettingsService.prototype.getHeaders = function () {
        var headersObj = {
            'Content-Type': 'application/json',
            'Authorization': ''
        };
        var token = this._sessionService.getToken();
        if (token != null) {
            headersObj.Authorization = 'Token ' + token;
        }
        return new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */](headersObj);
    };
    HttpSettingsService.prototype.getMultipartFormDataHeaders = function () {
        var headersObj = {
            'Content-Type': 'multipart/form-data',
            'Authorization': ''
        };
        var token = this._sessionService.getToken();
        if (token != null) {
            headersObj.Authorization = 'Token ' + token;
        }
        return new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */](headersObj);
    };
    HttpSettingsService.prototype.addTokenToParams = function (params) {
        if (typeof params === 'undefined') {
            params = {};
        }
        // Mark: Turned this off for now because it is extraneous.
        // let field = 'token';
        // params[field] = this._sessionService.getToken();
        return params;
    };
    HttpSettingsService.prototype.getUnauthorizedHeaders = function () {
        var headersObj = {
            'Content-Type': 'application/json'
        };
        return new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */](headersObj);
    };
    HttpSettingsService.prototype.getBaseUrl = function () {
        return this.protocol + '://' + this.domain + this.baseUrl;
    };
    HttpSettingsService.prototype.notLoggedIn = function () {
        this._sessionService.notLoggedIn();
    };
    return HttpSettingsService;
}());
HttpSettingsService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__session_service__["a" /* SessionService */], __WEBPACK_IMPORTED_MODULE_3__SettingsService__["a" /* SettingsService */]])
], HttpSettingsService);

//# sourceMappingURL=HttpSettingsService.js.map

/***/ }),

/***/ 98:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GymPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GymPage = (function () {
    function GymPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    return GymPage;
}());
GymPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-view-gym',template:/*ion-inline-start:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/view-gym/view-gym.html"*/'<ion-header no-border="">\n  <ion-navbar transparent style="color: #fff">\n    <button ion-button menuToggle style="color: #fff">\n      <ion-icon name="menu" style="color: #fff"></ion-icon>\n    </button>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding >\n  <div class="dark-gradient"></div>\n  <div >\n\n<div class="padding-top" >\n  <ion-card >\n    <ion-card-content>\n     <ion-card-title>\n        Thika Car Hire Limited\n      </ion-card-title>\n      </ion-card-content>\n    <img src="assets/images/3.jpg" >\n    <ion-card-content >\n      <ion-card-title>\n        Thika Car Hire Limited\n      </ion-card-title>\n\n      <p>\n        Lore Ipsum Lore Ipsum Lore Ipsum Lore Ipsum Lore Ipsum Lore Ipsum Lore Ipsum Lore Ipsum Lore Ipsum\n        Lore Ipsum Lore Ipsum Lore Ipsum Lore Ipsum Lore Ipsum Lore Ipsum Lore Ipsum Lore Ipsum Lore Ipsum Lore Ipsum\n        Lore Ipsum Lore Ipsum Lore Ipsum\n      </p>\n    </ion-card-content>\n\n    <ion-card-content >\n      <ion-card-title>\n      price per Day\n      </ion-card-title>\n      <p>\n        <ion-icon name=\'md-cash\' style="color: #28a74b;" ></ion-icon>\n          Ksh. 3000\n      </p>\n       <ion-card-title >\n        Location\n      </ion-card-title>\n      <p>\n        <ion-icon name=\'md-locate\' style="color: #2119ff;"></ion-icon>\n        Thika Town\n      </p>\n       <ion-card-title>\n        Contacts\n      </ion-card-title>\n      <p>\n        <ion-icon name=\'md-locate\' style="color: #2119ff;"></ion-icon>\n        0718555832\n      </p>\n      <p>\n        <ion-icon name=\'md-locate\' style="color: #2119ff;"></ion-icon>\n        thikatown@gmail.com\n      </p>\n\n    </ion-card-content>\n\n    <ion-row no-padding>\n      <ion-col>\n        <button ion-button clear small color="danger" icon-start>\n          <ion-icon name=\'star\'></ion-icon>\n          Favorite\n        </button>\n      </ion-col>\n      <ion-col text-center>\n        <button ion-button clear small color="danger" icon-start>\n          <ion-icon name=\'contacts\'></ion-icon>\n          Connect\n        </button>\n      </ion-col>\n      <ion-col text-right>\n        <button ion-button clear small color="danger" icon-start>\n          <ion-icon name=\'share-alt\'></ion-icon>\n          Share\n        </button>\n      </ion-col>\n    </ion-row>\n\n  </ion-card>\n</div>\n    </div>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/Car-Hire/client-car-hire/src/pages/view-gym/view-gym.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
], GymPage);

//# sourceMappingURL=view-gym.js.map

/***/ })

},[348]);
//# sourceMappingURL=main.js.map