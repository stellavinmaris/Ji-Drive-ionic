webpackJsonp([0],{

/***/ 142:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signup_signup__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__reset_password_reset_password__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__products_products__ = __webpack_require__(80);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






 //Added reset password page
var HomePage = (function () {
    function HomePage(navCtrl, fb, auth) {
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.auth = auth;
        this.signupPage = __WEBPACK_IMPORTED_MODULE_4__signup_signup__["a" /* SignupPage */];
        this.resetPasswordPage = __WEBPACK_IMPORTED_MODULE_5__reset_password_reset_password__["a" /* ResetPasswordPage */]; //Added reset password page
        this.loginForm = this.fb.group({
            'email': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)])],
            'password': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].minLength(1)])]
        });
        this.email = this.loginForm.controls['email'];
        this.password = this.loginForm.controls['password'];
    }
    HomePage.prototype.login = function () {
        var _this = this;
        if (this.loginForm.valid) {
            var credentials = ({ email: this.email.value, password: this.password.value });
            this.auth.loginWithEmail(credentials).subscribe(function (data) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__products_products__["a" /* ProductsPage */]);
                console.log(data);
            }, function (error) {
                console.log(error);
                if (error.code == 'auth/user-not-found') {
                    alert('User not found');
                }
                else {
                    if (error.code == 'auth/wrong-password') {
                        alert('The password is invalid or the user does not have a password.');
                    }
                }
            });
        }
    };
    HomePage.prototype.logout = function () {
        this.auth.logout();
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"/Users/stella/PycharmProjects/ionic-starter/src/pages/login/login.html"*/'\n<ion-content padding>\n  <div class="dark-gradient"></div>\n    <!--<ion-title>Login</ion-title>-->\n\n    <form [formGroup]="loginForm" (ngSubmit)="submit()" novalidate class="padding-top">\n        <ion-row>\n            <ion-item>\n                <ion-label for="email"></ion-label>\n                <ion-input type="email" value="" placeholder="Email" formControlName="email"></ion-input>\n            </ion-item>\n        </ion-row>\n        <ion-row>\n            <ion-item>\n                <ion-label for="password"></ion-label>\n                <ion-input type="password"  placeholder="Password" formControlName="password"></ion-input>\n            </ion-item>\n        </ion-row>\n    </form>\n\n      <div class="login-action-btns" padding-top>\n        <button [navPush]="signupPage" class="register-button" color="light" ion-button block outline="">Sign up</button>\n        <button (click)="login()" class="login-button" color="light" ion-button block outline="">Log in</button>\n      </div>\n\n    <!--new link to sign up page-->\n    <ion-row>\n        <button [navPush]="resetPasswordPage"  color="light" class="border" ion-button block outline>Reset password</button>\n    </ion-row>\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/ionic-starter/src/pages/login/login.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__["a" /* AuthProvider */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FitnessCentersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signup_signup__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__reset_password_reset_password__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__list_gym_list_gym__ = __webpack_require__(82);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






 //Added reset password page
var FitnessCentersPage = (function () {
    function FitnessCentersPage(navCtrl, fb, auth) {
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.auth = auth;
        this.signupPage = __WEBPACK_IMPORTED_MODULE_4__signup_signup__["a" /* SignupPage */];
        this.resetPasswordPage = __WEBPACK_IMPORTED_MODULE_5__reset_password_reset_password__["a" /* ResetPasswordPage */]; //Added reset password page
    }
    FitnessCentersPage.prototype.goToGym = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__list_gym_list_gym__["a" /* ListGymPage */]);
    };
    return FitnessCentersPage;
}());
FitnessCentersPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'fitness-centers',template:/*ion-inline-start:"/Users/stella/PycharmProjects/ionic-starter/src/pages/fitness-centers/fitness-centers.html"*/'\n<ion-header no-border="">\n  <ion-navbar transparent>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Fitness Centers</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding >\n    <div class="dark-gradient"></div>\n<ion-grid style="margin-top: 100px">\n    <ion-row >\n      <ion-col col-lg-12 push-lg-0 col-md-12 push-md-0 no-padding>\n        <div>\n          <ion-row no-padding class="discovery-row">\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h1>Aerobic Centers</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h1>Yoga Centers</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h1>Dance Centers</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h1>Pilates Centers</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h1>Gyms Centers</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h1>Athletic Clubs</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <!--<ion-card style="margin-top: 100px" (click)="goToGym()">-->\n    <!--<img src="assets/imgs/14.jpg" class="image"/>-->\n    <!--<div class="card-subtitle">Inspirational content</div>-->\n    <!--<div class="card-title"> Aerobic Centers</div>-->\n  <!--</ion-card>-->\n\n  <!--<ion-card>-->\n    <!--<img src="assets/imgs/20.jpg" class="image"/>-->\n    <!--<<div class="card-subtitle">Inspirational content</div>-->\n    <!--<div class="card-title">Yoga Centers</div>-->\n  <!--</ion-card>-->\n\n  <!--<ion-card>-->\n    <!--<img src="assets/imgs/13.jpg" class="image"/>-->\n    <!--<div class="card-subtitle">Inspirational content</div>-->\n    <!--<div class="card-title">Dance Centers</div>-->\n  <!--</ion-card>-->\n\n   <!--<ion-card (click)="goToGym()">-->\n    <!--<img src="assets/imgs/14.jpg" class="image"/>-->\n    <!--<div class="card-subtitle">Inspirational content</div>-->\n    <!--<div class="card-title"> Pilates Centers</div>-->\n  <!--</ion-card>-->\n\n  <!--<ion-card>-->\n    <!--<img src="assets/imgs/20.jpg" class="image"/>-->\n    <!--<<div class="card-subtitle">Inspirational content</div>-->\n    <!--<div class="card-title">Gyms</div>-->\n  <!--</ion-card>-->\n\n  <!--<ion-card  (click)="goToGym()">-->\n    <!--<img src="assets/imgs/14.jpg" class="image"/>-->\n    <!--<div class="card-subtitle">Inspirational content</div>-->\n    <!--<div class="card-title"> Country Clubs</div>-->\n  <!--</ion-card>-->\n\n  <!--<ion-card>-->\n    <!--<img src="assets/imgs/20.jpg" class="image"/>-->\n    <!--<<div class="card-subtitle">Inspirational content</div>-->\n    <!--<div class="card-title">Athletic Clubs</div>-->\n  <!--</ion-card>-->\n\n\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/ionic-starter/src/pages/fitness-centers/fitness-centers.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__["a" /* AuthProvider */]])
], FitnessCentersPage);

//# sourceMappingURL=fitness-centers.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WellnessCenterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__list_gym_list_gym__ = __webpack_require__(82);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var WellnessCenterPage = (function () {
    function WellnessCenterPage(navCtrl, fb, auth) {
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.auth = auth;
    }
    WellnessCenterPage.prototype.goToGym = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__list_gym_list_gym__["a" /* ListGymPage */]);
    };
    return WellnessCenterPage;
}());
WellnessCenterPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'wellness-center',template:/*ion-inline-start:"/Users/stella/PycharmProjects/ionic-starter/src/pages/wellness-centers/wellness-center.html"*/'<script src="fitness-centers.ts"></script>\n<ion-header no-border="">\n  <ion-navbar transparent>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Wellness Centers</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding >\n    <div class="dark-gradient"></div>\n<ion-grid style="margin-top: 100px">\n    <ion-row >\n      <ion-col col-lg-12 push-lg-0 col-md-12 push-md-0 no-padding>\n        <div>\n          <ion-row no-padding class="discovery-row">\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h3>Centers for General Health and Well-Being </h3>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h3>Centers that offer Specific Health Services</h3>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n\n\n            <ion-col class="discovery-col" col-xl-2 col-lg-3 col-md-4 col-6 no-padding (tap)="goToGym()">\n              <div class="discovery-card">\n                <br>\n                <h3>Well-Being Centers Run By Physicians</h3>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n\n          </ion-row>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/ionic-starter/src/pages/wellness-centers/wellness-center.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__["a" /* AuthProvider */]])
], WellnessCenterPage);

//# sourceMappingURL=wellness-center.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(317);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ImageProvider = (function () {
    function ImageProvider(http, _CAMERA) {
        this.http = http;
        this._CAMERA = _CAMERA;
    }
    ImageProvider.prototype.selectImage = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var cameraOptions = {
                sourceType: _this._CAMERA.PictureSourceType.PHOTOLIBRARY,
                destinationType: _this._CAMERA.DestinationType.DATA_URL,
                quality: 100,
                targetWidth: 320,
                targetHeight: 240,
                encodingType: _this._CAMERA.EncodingType.JPEG,
                correctOrientation: true
            };
            _this._CAMERA.getPicture(cameraOptions)
                .then(function (data) {
                _this.cameraImage = "data:image/jpeg;base64," + data;
                resolve(_this.cameraImage);
            });
        });
    };
    return ImageProvider;
}());
ImageProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */]])
], ImageProvider);

//# sourceMappingURL=image.js.map

/***/ }),

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PreloaderProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PreloaderProvider = (function () {
    function PreloaderProvider(http, loadingCtrl) {
        this.http = http;
        this.loadingCtrl = loadingCtrl;
    }
    PreloaderProvider.prototype.displayPreloader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
    };
    PreloaderProvider.prototype.hidePreloader = function () {
        this.loading.dismiss();
    };
    return PreloaderProvider;
}());
PreloaderProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* LoadingController */]])
], PreloaderProvider);

//# sourceMappingURL=preloader.js.map

/***/ }),

/***/ 171:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 171;

/***/ }),

/***/ 21:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

 //Add FirebaseApp

var AuthProvider = (function () {
    function AuthProvider(af) {
        this.af = af;
    }
    AuthProvider.prototype.loginWithEmail = function (credentials) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].create(function (observer) {
            _this.af.auth.signInWithEmailAndPassword(credentials.email, credentials.password).then(function (authData) {
                //console.log(authData);
                observer.next(authData);
            }).catch(function (error) {
                observer.error(error);
            });
        });
    };
    AuthProvider.prototype.registerUser = function (credentials) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].create(function (observer) {
            _this.af.auth.createUserWithEmailAndPassword(credentials.email, credentials.password).then(function (authData) {
                //authData.auth.updateProfile({displayName: credentials.displayName, photoURL: credentials.photoUrl}); //set name and photo
                observer.next(authData);
            }).catch(function (error) {
                //console.log(error);
                observer.error(error);
            });
        });
    };
    AuthProvider.prototype.resetPassword = function (emailAddress) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].create(function (observer) {
            _this.af.auth.sendPasswordResetEmail(emailAddress).then(function (success) {
                //console.log('email sent', success);
                observer.next(success);
            }, function (error) {
                //console.log('error sending email',error);
                observer.error(error);
            });
        });
    };
    AuthProvider.prototype.logout = function () {
        this.af.auth.signOut();
    };
    Object.defineProperty(AuthProvider.prototype, "currentUser", {
        get: function () {
            return this.af.auth.currentUser ? this.af.auth.currentUser.email : null;
        },
        enumerable: true,
        configurable: true
    });
    return AuthProvider;
}());
AuthProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["a" /* AngularFireAuth */]])
], AuthProvider);

//# sourceMappingURL=auth-provider.js.map

/***/ }),

/***/ 214:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 214;

/***/ }),

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GymPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_provider__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var GymPage = (function () {
    function GymPage(navCtrl, navParams, af, auth, _toastCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.af = af;
        this.auth = auth;
        this._toastCtrl = _toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.gymResponse = [];
        this.GymData = [];
        this.rawList = [];
        this.gymId = this.navParams.get('gymId');
        this.GymData = __WEBPACK_IMPORTED_MODULE_4_firebase__["database"]().ref('/fitness-centers/' + this.gymId);
        if (this.gymId) {
            this.getGymData();
        }
    }
    GymPage.prototype.showToast = function (msg, duration) {
        if (duration === void 0) { duration = 4000; }
        var toast = this._toastCtrl.create({
            message: msg,
            duration: duration
        });
    };
    GymPage.prototype.showLoading = function () {
        this.loadingController = this.loadingCtrl.create({
            content: 'Loading'
        });
        this.loadingController.present();
    };
    GymPage.prototype.hideLoading = function () {
        this.loadingController.dismiss();
    };
    GymPage.prototype.getGymData = function () {
        this.showLoading();
        var rawList = [];
        this.GymData.once('value').then(function (spanshot) {
            var cat = spanshot.val();
            rawList.push({
                $key: spanshot.key,
                title: cat.title,
                location: cat.location,
                category: cat.category,
                image: cat.image,
                price: cat.price,
                // phone: cat.phone,
                // email: cat.email,
                description: cat.description
            });
        });
        this.gymResponse = rawList;
        console.log('one', this.gymResponse);
        this.hideLoading();
    };
    return GymPage;
}());
GymPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-view-gym',template:/*ion-inline-start:"/Users/stella/PycharmProjects/ionic-starter/src/pages/view-gym/view-gym.html"*/'<ion-header no-border="">\n  <ion-navbar transparent style="color: #fff">\n    <button ion-button menuToggle style="color: #fff">\n      <ion-icon name="menu" style="color: #fff"></ion-icon>\n    </button>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding *ngIf="loadingController" >\n  <div class="dark-gradient"></div>\n  <div *ngIf="gymResponse && gymId">\n\n<div class="padding-top" *ngFor="let data of gymResponse" >\n  <ion-card *ngIf="gymId == data.$key">\n    <ion-card-content>\n     <ion-card-title>\n        {{data.title}}\n      </ion-card-title>\n      </ion-card-content>\n    <img [src]="data.image" >\n    <ion-card-content >\n      <ion-card-title>\n        About {{data.title}}\n      </ion-card-title>\n\n      <p>\n        {{data.description}}\n      </p>\n    </ion-card-content>\n\n    <ion-card-content >\n      <ion-card-title>\n      price per Month\n      </ion-card-title>\n      <p>\n        <ion-icon name=\'md-cash\' style="color: #28a74b;" ></ion-icon>\n          Ksh. {{data.price}}\n      </p>\n       <ion-card-title >\n        Location\n      </ion-card-title>\n      <p *ngFor="let location of data.location">\n        <ion-icon name=\'md-locate\' style="color: #2119ff;"></ion-icon>\n        {{location.name}}\n      </p>\n       <ion-card-title>\n        Contacts\n      </ion-card-title>\n\n    </ion-card-content>\n\n    <ion-row no-padding>\n      <ion-col>\n        <button ion-button clear small color="danger" icon-start>\n          <ion-icon name=\'star\'></ion-icon>\n          Favorite\n        </button>\n      </ion-col>\n      <ion-col text-center>\n        <button ion-button clear small color="danger" icon-start>\n          <ion-icon name=\'contacts\'></ion-icon>\n          Connect\n        </button>\n      </ion-col>\n      <ion-col text-right>\n        <button ion-button clear small color="danger" icon-start>\n          <ion-icon name=\'share-alt\'></ion-icon>\n          Share\n        </button>\n      </ion-col>\n    </ion-row>\n\n  </ion-card>\n</div>\n    </div>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/ionic-starter/src/pages/view-gym/view-gym.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_2__providers_auth_provider__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* LoadingController */]])
], GymPage);

//# sourceMappingURL=view-gym.js.map

/***/ }),

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddGymPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(83);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddGymPage = AddGymPage_1 = (function () {
    function AddGymPage(navCtrl, navParams, af, fb, auth, _toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.af = af;
        this.fb = fb;
        this.auth = auth;
        this._toastCtrl = _toastCtrl;
        this.AddGymList = af.database.ref('/gym');
    }
    AddGymPage.prototype.showToast = function (msg, duration) {
        if (duration === void 0) { duration = 4000; }
        var toast = this._toastCtrl.create({
            message: msg,
            duration: duration
        });
    };
    AddGymPage.prototype.addGym = function (name, location, phone, email, description) {
        var _this = this;
        this.AddGymList.push({
            name: name,
            location: location,
            phone: phone,
            email: email,
            description: description,
        }).then(function (newContact) {
            _this.showToast("Gym details saved");
            _this.navCtrl.setRoot(AddGymPage_1);
        }, function (error) {
            console.log(error);
        });
    };
    return AddGymPage;
}());
AddGymPage = AddGymPage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-add-gym',template:/*ion-inline-start:"/Users/stella/PycharmProjects/ionic-starter/src/pages/add-gym/add-gym.html"*/'<ion-header no-border="">\n  <ion-navbar transparent>\n    <button ion-button menuToggle style="color: #fff">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Add Gym</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div class="dark-gradient"></div>\n   <ion-grid fixed class="padding-top">\n        <ion-row>\n            <ion-col col-lg-6 push-lg-3 col-md-6 push-md-3 col-sm-12>\n              <!--<ion-title>Signup</ion-title>-->\n              <form novalidate>\n                  <ion-row>\n                    <ion-item>\n                        <ion-label for="name"></ion-label>\n                        <ion-input [(ngModel)]="name" type="text" name="name" value="" placeholder="Gym Name" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n\n                <ion-row>\n                    <ion-item>\n                        <ion-label for="location"></ion-label>\n                        <ion-input [(ngModel)]="location" name="location" type="text" value="" placeholder="location" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n\n                <ion-row>\n                    <ion-item>\n                        <ion-label for="phone"></ion-label>\n                        <ion-input [(ngModel)]="phone" type="text" name="phone" value="" placeholder="Phone Number" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n\n                <ion-row>\n                    <ion-item>\n                        <ion-label for="email"></ion-label>\n                        <ion-input [(ngModel)]="email" type="email" name="email" value="" placeholder="email" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n\n                <ion-row>\n                    <ion-item>\n                        <ion-label for="description"></ion-label>\n                        <ion-input [(ngModel)]="description" type="text" name="description" value="" placeholder="description" ></ion-input>\n                    </ion-item>\n                  </ion-row>\n\n\n                <br><br>\n\n                  <ion-row>\n                      <button  (click)="addGym(name,location,phone,email,description)" class="register-button" ion-button type="submit" color="light" block outline>Add Gym</button>\n                  </ion-row>\n              </form>\n            </ion-col>\n        </ion-row>\n   </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/ionic-starter/src/pages/add-gym/add-gym.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* ToastController */]])
], AddGymPage);

var AddGymPage_1;
//# sourceMappingURL=add-gym.js.map

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImagePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_services_image__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_services_preloader__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_services_database__ = __webpack_require__(318);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ImagePage = ImagePage_1 = (function () {
    function ImagePage(navCtrl, params, _FB, _IMG, viewCtrl, _LOADER, _DB) {
        this.navCtrl = navCtrl;
        this.params = params;
        this._FB = _FB;
        this._IMG = _IMG;
        this.viewCtrl = viewCtrl;
        this._LOADER = _LOADER;
        this._DB = _DB;
        this.fitnessName = '';
        this.fitnessImage = '';
        this.fitnessCategory = [];
        this.fitnessPrice = '';
        this.fitnessDescription = '';
        this.fitnessLocation = [];
        this.fitnessId = '';
        this.isEditable = false;
        this.form = _FB.group({
            'description': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].minLength(10)],
            'name': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            'price': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            'image': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            'category': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            'location': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required]
        });
        this.fitness = __WEBPACK_IMPORTED_MODULE_3_firebase__["database"]().ref('fitness-centers/');
        if (params.get('isEdited')) {
            var fitness = params.get('fitness'), k = void 0;
            this.fitnessName = fitness.title;
            this.fitnessPrice = fitness.price;
            this.fitnessDescription = fitness.description;
            this.fitnessImage = fitness.image;
            this.fitnessId = fitness.id;
            for (k in fitness.category) {
                this.fitnessCategory.push(fitness.category[k].name);
            }
            for (k in fitness.location) {
                this.fitnessLocation.push(fitness.location[k].name);
            }
            this.isEditable = true;
        }
    }
    ImagePage.prototype.saveMovie = function (val) {
        var _this = this;
        this._LOADER.displayPreloader();
        var title = this.form.controls["name"].value, description = this.form.controls["description"].value, category = this.form.controls["category"].value, location = this.form.controls["location"].value, price = this.form.controls["price"].value, image = this.fitnessCenterImage, types = [], people = [], k;
        for (k in category) {
            types.push({
                "name": category[k]
            });
        }
        for (k in location) {
            people.push({
                "name": location[k]
            });
        }
        if (this.isEditable) {
            if (image !== this.fitnessImage) {
                this._DB.uploadImage(image)
                    .then(function (snapshot) {
                    var uploadedImage = snapshot.downloadURL;
                    _this._DB.updateDatabase(_this.fitnessId, {
                        title: title,
                        description: description,
                        price: price,
                        image: uploadedImage,
                        category: types,
                        location: people
                    })
                        .then(function (data) {
                        _this._LOADER.hidePreloader();
                    });
                });
            }
            else {
                this._DB.updateDatabase(this.fitnessId, {
                    title: title,
                    description: description,
                    price: price,
                    category: types,
                    location: people
                })
                    .then(function (data) {
                    _this._LOADER.hidePreloader();
                });
            }
        }
        else {
            this._DB.uploadImage(image)
                .then(function (snapshot) {
                var uploadedImage = snapshot.downloadURL;
                _this._DB.addToDatabase({
                    title: title,
                    image: uploadedImage,
                    description: description,
                    price: price,
                    category: types,
                    location: people
                })
                    .then(function (data) {
                    _this._LOADER.hidePreloader();
                    _this.navCtrl.setRoot(ImagePage_1);
                });
            });
        }
        this.closeModal(true);
    };
    ImagePage.prototype.closeModal = function (val) {
        if (val === void 0) { val = null; }
        this.viewCtrl.dismiss(val);
    };
    ImagePage.prototype.selectImage = function () {
        var _this = this;
        this._IMG.selectImage()
            .then(function (data) {
            _this.fitnessCenterImage = data;
        });
    };
    return ImagePage;
}());
ImagePage = ImagePage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-modals',template:/*ion-inline-start:"/Users/stella/PycharmProjects/ionic-starter/src/pages/image/home.html"*/'<ion-header >\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n         <!--{{ title }}--> Add Wellness Center\n      </ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content padding>\n\n <ion-item-divider color="light">\n         <div *ngIf="!isEditable">\n            Add Product\n         </div>\n\n         <div *ngIf="isEditable">\n            Amend this Product\n         </div>\n   	  </ion-item-divider>\n\n  <ion-toolbar>\n\n      <ion-buttons start>\n         <button\n            ion-button\n            (click)="closeModal()">\n           <span\n              ion-text\n              color="primary"\n              showWhen="ios">Cancel</span>\n           <ion-icon\n              name="md-close"\n              showWhen="android,windows"></ion-icon>\n         </button>\n      </ion-buttons>\n   </ion-toolbar>\n   <form\n      [formGroup]="form"\n      (ngSubmit)="saveMovie(form.value)">\n\n\n   	  <ion-item>\n         <ion-label stacked>Center Name:</ion-label>\n         <ion-input\n            type="text"\n            formControlName="name"\n            [(ngModel)]="fitnessName"></ion-input>\n   	  </ion-item>\n\n\n\n   	  <ion-item>\n         <span\n            ion-text\n            color="danger"\n            block\n            text-center\n            padding-top\n            padding-bottom\n            (click)="selectImage()">Select an image</span>\n            <input\n               type="hidden"\n               name="image"\n               formControlName="image"\n               [(ngModel)]="fitnessImage">\n            <img [src]="fitnessCenterImage">\n   	  </ion-item>\n\n\n\n   	  <ion-item>\n         <ion-label stacked>Price Per Month:</ion-label>\n         <ion-input\n            type="text"\n            formControlName="price"\n            [(ngModel)]="fitnessPrice"></ion-input>\n   	  </ion-item>\n\n\n\n   	  <ion-item>\n         <ion-label stacked>Category:</ion-label>\n         <ion-select\n            formControlName="category"\n            multiple="true"\n            [(ngModel)]="fitnessCategory">\n             <ion-option value="Aerobic">Aerobic</ion-option>\n              <ion-option value="Athletic">Athletic</ion-option>\n               <ion-option value="Dance">Dance</ion-option>\n               <ion-option value="Gyms">Gyms</ion-option>\n               <ion-option value="Pilates">Pilates</ion-option>\n               <ion-option value="Yoga">Yoga</ion-option>\n\n\n         </ion-select>\n   	  </ion-item>\n\n\n\n   	  <ion-item>\n         <ion-label stacked>Location:</ion-label>\n         <ion-select\n            formControlName="location"\n            multiple="true"\n            [(ngModel)]="fitnessLocation">\n             <ion-option value="Karen">Karen</ion-option>\n             <ion-option value="Karen Hardy">Karen Hardy</ion-option>\n             <ion-option value="Runda">Runda</ion-option>\n             <ion-option value="Ngong Road">Ngong Road</ion-option>\n             <ion-option value="Kasarani">Kasarani</ion-option>\n             <ion-option value="Kiambu">Kianbu</ion-option>\n             <ion-option value="Thika">Thika</ion-option>\n             <ion-option value="umoja">Umoja</ion-option>\n             <ion-option value="Utawala">Utawala</ion-option>\n             <ion-option value="Roysambu">Roysambu</ion-option>\n             <ion-option value="Mwiki">Mwiki</ion-option>\n             <ion-option value="Juja">Juja</ion-option>\n             <ion-option value="Kahawa Wendani">Kahawa Wendani</ion-option>\n             <ion-option value="Kahawa Sukari">Kahawa Sukari</ion-option>\n             <ion-option value="Kahawa West">Kahawa West</ion-option>\n             <ion-option value="Nairobi West">Nairobi West</ion-option>\n             <ion-option value="Parklands">Parklands </ion-option>\n             <ion-option value="Westlands">Westlands</ion-option>\n             <ion-option value="Muthaiga">Muthaiga</ion-option>\n             <ion-option value="Githurai">Githurai</ion-option>\n             <ion-option value="Limuru">Limuru</ion-option>\n             <ion-option value="Kikuyu">Kikuyu</ion-option>\n             <ion-option value="Embakasi">Embakasi</ion-option>\n         </ion-select>\n   	  </ion-item>\n\n\n\n   	  <ion-item>\n         <ion-label stacked>Description:</ion-label>\n         <ion-textarea\n			rows="6"\n            formControlName="description"\n            [(ngModel)]="fitnessDescription"></ion-textarea>\n   	  </ion-item>\n\n\n\n   	  <ion-item>\n   	     <input\n   	        type="hidden"\n   	        name="fitnessId">\n         <button\n           ion-button\n           block\n           color="primary"\n           text-center\n           padding-top\n           padding-bottom\n           >\n            <div *ngIf="!isEditable">\n               Save Wellness Center\n            </div>\n\n            </button>\n   	  </ion-item>\n\n   </form>\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/ionic-starter/src/pages/image/home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4__shared_services_image__["a" /* ImageProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_5__shared_services_preloader__["a" /* PreloaderProvider */],
        __WEBPACK_IMPORTED_MODULE_6__shared_services_database__["a" /* DatabaseProvider */]])
], ImagePage);

var ImagePage_1;
//# sourceMappingURL=home.js.map

/***/ }),

/***/ 318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatabaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DatabaseProvider = (function () {
    function DatabaseProvider(http) {
        this.http = http;
    }
    DatabaseProvider.prototype.renderMovies = function () {
        return new __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"](function (observer) {
            var films = [];
            __WEBPACK_IMPORTED_MODULE_4_firebase__["database"]().ref('fitness-centers').orderByKey().once('value', function (items) {
                items.forEach(function (item) {
                    films.push({
                        id: item.key,
                        location: item.val().location,
                        price: item.val().price,
                        category: item.val().category,
                        image: item.val().image,
                        description: item.val().description,
                        title: item.val().title
                    });
                });
                observer.next(films);
                observer.complete();
            }, function (error) {
                console.log("Observer error: ", error);
                console.dir(error);
                observer.error(error);
            });
        });
    };
    DatabaseProvider.prototype.deleteMovie = function (id) {
        return new Promise(function (resolve) {
            var ref = __WEBPACK_IMPORTED_MODULE_4_firebase__["database"]().ref('fitness-centers').child(id);
            ref.remove();
            resolve(true);
        });
    };
    DatabaseProvider.prototype.addToDatabase = function (movieObj) {
        return new Promise(function (resolve) {
            var addRef = __WEBPACK_IMPORTED_MODULE_4_firebase__["database"]().ref('fitness-centers');
            addRef.push(movieObj);
            resolve(true);
        });
    };
    DatabaseProvider.prototype.updateDatabase = function (id, moviesObj) {
        return new Promise(function (resolve) {
            var updateRef = __WEBPACK_IMPORTED_MODULE_4_firebase__["database"]().ref('fitness-centers').child(id);
            updateRef.update(moviesObj);
            resolve(true);
        });
    };
    DatabaseProvider.prototype.uploadImage = function (imageString) {
        var image = 'movie-' + new Date().getTime() + '.jpg', storageRef, parseUpload;
        return new Promise(function (resolve, reject) {
            storageRef = __WEBPACK_IMPORTED_MODULE_4_firebase__["storage"]().ref('fitness-centers/' + image);
            parseUpload = storageRef.putString(imageString, 'data_url');
            parseUpload.on('state_changed', function (_snapshot) {
                // We could log the progress here IF necessary
                // console.log('snapshot progess ' + _snapshot);
            }, function (_err) {
                reject(_err);
            }, function (success) {
                resolve(parseUpload.snapshot);
            });
        });
    };
    return DatabaseProvider;
}());
DatabaseProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
], DatabaseProvider);

//# sourceMappingURL=database.js.map

/***/ }),

/***/ 319:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_services_image__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_services_register_service__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_services_preloader__ = __webpack_require__(161);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ProfilePage = ProfilePage_1 = (function () {
    function ProfilePage(navCtrl, navParams, fb, auth, _imageService, _registerService, _LOADER, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.auth = auth;
        this._imageService = _imageService;
        this._registerService = _registerService;
        this._LOADER = _LOADER;
        this.viewCtrl = viewCtrl;
        this.pfImage = '';
        this.signupForm = this.fb.group({
            'image': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            'username': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].minLength(2)])],
            'phonenumber': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].minLength(9)])],
            'sex': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            'firstname': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].minLength(2)])],
            'lastname': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].minLength(2)])],
        });
        this.firstname = this.signupForm.controls['firstname'];
        this.lastname = this.signupForm.controls['lastname'];
        this.username = this.signupForm.controls['username'];
        this.phonenumber = this.signupForm.controls['phonenumber'];
        this.image = this.signupForm.controls['image'];
    }
    ProfilePage.prototype.saveprofile = function (val) {
        var _this = this;
        this._LOADER.displayPreloader();
        var username = this.signupForm.controls["username"].value, phonenumber = this.signupForm.controls["phonenumber"].value, sex = this.signupForm.controls["sex"].value, firstname = this.signupForm.controls["firstname"].value, lastname = this.signupForm.controls["lastname"].value, image = this.ProfileImage, userId = this.auth.currentUser, gender = [], k;
        for (k in sex) {
            gender.push({
                "gender": sex[k]
            });
        }
        this._registerService.uploadImage(image)
            .then(function (snapshot) {
            var uploadedImage = snapshot.downloadURL;
            _this._registerService.addToDatabase({
                username: username,
                image: uploadedImage,
                sex: gender,
                firstname: firstname,
                lastname: lastname,
                phonenumber: phonenumber,
                userId: userId
            })
                .then(function (data) {
                _this._LOADER.hidePreloader();
                _this.navCtrl.setRoot(ProfilePage_1);
            });
        });
        this.closeModal(true);
    };
    ProfilePage.prototype.closeModal = function (val) {
        if (val === void 0) { val = null; }
        this.viewCtrl.dismiss(val);
    };
    ProfilePage.prototype.selectImage = function () {
        var _this = this;
        this._imageService.selectImage()
            .then(function (data) {
            _this.ProfileImage = data;
        });
    };
    return ProfilePage;
}());
ProfilePage = ProfilePage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-profile',template:/*ion-inline-start:"/Users/stella/PycharmProjects/ionic-starter/src/pages/profile/profile.html"*/'<ion-header no-border="">\n  <ion-navbar transparent>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Edit Profile</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <div class="dark-gradient"></div>\n   <ion-grid fixed class="padding-top">\n        <ion-row>\n            <ion-col col-lg-6 push-lg-3 col-md-6 push-md-3 col-sm-12>\n              <!--<ion-title>Signup</ion-title>-->\n              <form [formGroup]="signupForm" (ngSubmit)="saveprofile()" novalidate>\n\n                    <ion-item>\n                        <ion-label for="firstname"></ion-label>\n                        <ion-input type="text" value="" placeholder="First Name" formControlName="firstname"></ion-input>\n                    </ion-item>\n\n                    <ion-item>\n                        <ion-label for="lastname"></ion-label>\n                        <ion-input type="text" value="" placeholder="Last Name" formControlName="lastname"></ion-input>\n                    </ion-item>\n\n                    <ion-item>\n                        <ion-label for="username"></ion-label>\n                        <ion-input type="text" value="" placeholder="Username" formControlName="username"></ion-input>\n                    </ion-item>\n\n                    <ion-item>\n                        <ion-label for="phonenumber"></ion-label>\n                        <ion-input type="text" value="" placeholder="Phone Number" formControlName="phonenumber"></ion-input>\n                    </ion-item>\n\n                <ion-item>\n                   <ion-label stacked>Gender:</ion-label>\n                   <ion-select\n                      formControlName="sex"\n                      >\n                       <ion-option value="Female">Female</ion-option>\n                        <ion-option value="Male">Male</ion-option>\n                         <ion-option value="Others">Others</ion-option>\n                   </ion-select>\n                </ion-item>\n\n                    <ion-item>\n                          <span\n                          ion-text\n                          color="primary"\n                          block\n                          text-center\n                          padding-top\n                          padding-bottom\n                          (click)="selectImage()">Select Profile Picture</span>\n                          <input\n                             type="hidden"\n                             name="image"\n                             formControlName="image"\n                             [(ngModel)]="pfImage">\n                          <img [src]="ProfileImage">\n                        </ion-item>\n\n                <br><br>\n\n                  <ion-row>\n                      <button class="register-button" ion-button type="submit" color="light" block outline>Sign up</button>\n                  </ion-row>\n              </form>\n            </ion-col>\n        </ion-row>\n   </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/ionic-starter/src/pages/profile/profile.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_4__shared_services_image__["a" /* ImageProvider */],
        __WEBPACK_IMPORTED_MODULE_5__shared_services_register_service__["a" /* RegisterService */],
        __WEBPACK_IMPORTED_MODULE_6__shared_services_preloader__["a" /* PreloaderProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* ViewController */]])
], ProfilePage);

var ProfilePage_1;
//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 320:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegisterService = (function () {
    function RegisterService(http) {
        this.http = http;
    }
    RegisterService.prototype.uploadImage = function (imageString) {
        var image = 'movie-' + new Date().getTime() + '.jpg', storageRef, parseUpload;
        return new Promise(function (resolve, reject) {
            storageRef = __WEBPACK_IMPORTED_MODULE_3_firebase__["storage"]().ref('user-profiles/' + image);
            parseUpload = storageRef.putString(imageString, 'data_url');
            parseUpload.on('state_changed', function (_snapshot) {
            }, function (_err) {
                reject(_err);
            }, function (success) {
                resolve(parseUpload.snapshot);
            });
        });
    };
    RegisterService.prototype.addToDatabase = function (movieObj) {
        return new Promise(function (resolve) {
            var addRef = __WEBPACK_IMPORTED_MODULE_3_firebase__["database"]().ref('user-profiles');
            addRef.push(movieObj);
            resolve(true);
        });
    };
    return RegisterService;
}());
RegisterService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
], RegisterService);

//# sourceMappingURL=register.service.js.map

/***/ }),

/***/ 321:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(337);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 337:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export firebaseConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(254);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__ = __webpack_require__(256);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angularfire2_auth__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_auth_provider__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_products_products__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_reset_password_reset_password__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_signup_signup__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_add_gym_add_gym__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_angularfire2_database__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_list_gym_list_gym__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_view_gym_view_gym__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__shared_services_database__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__shared_services_image__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__shared_services_preloader__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_image_home__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__angular_http__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_camera__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_fitness_centers_fitness_centers__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_wellness_centers_wellness_center__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__shared_services_register_service__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_profile_profile__ = __webpack_require__(319);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



























var firebaseConfig = {
    apiKey: "AIzaSyC6A-d2VJwK164tyV-YiA6vRBFb1xUkt7s",
    authDomain: "fir-project-e5251.firebaseapp.com",
    databaseURL: "https://fir-project-e5251.firebaseio.com",
    projectId: "fir-project-e5251",
    storageBucket: "gs://fir-project-e5251.appspot.com/",
    messagingSenderId: "902921514274"
};
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_products_products__["a" /* ProductsPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_reset_password_reset_password__["a" /* ResetPasswordPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_signup_signup__["a" /* SignupPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_add_gym_add_gym__["a" /* AddGymPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_list_gym_list_gym__["a" /* ListGymPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_view_gym_view_gym__["a" /* GymPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_image_home__["a" /* ImagePage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_fitness_centers_fitness_centers__["a" /* FitnessCentersPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_wellness_centers_wellness_center__["a" /* WellnessCenterPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_profile_profile__["a" /* ProfilePage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_21__angular_http__["b" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                links: []
            }),
            __WEBPACK_IMPORTED_MODULE_7_angularfire2__["a" /* AngularFireModule */].initializeApp(firebaseConfig),
            __WEBPACK_IMPORTED_MODULE_8_angularfire2_auth__["b" /* AngularFireAuthModule */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_products_products__["a" /* ProductsPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_reset_password_reset_password__["a" /* ResetPasswordPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_signup_signup__["a" /* SignupPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_add_gym_add_gym__["a" /* AddGymPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_list_gym_list_gym__["a" /* ListGymPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_view_gym_view_gym__["a" /* GymPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_image_home__["a" /* ImagePage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_fitness_centers_fitness_centers__["a" /* FitnessCentersPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_wellness_centers_wellness_center__["a" /* WellnessCenterPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_profile_profile__["a" /* ProfilePage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_9__providers_auth_provider__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_14_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_17__shared_services_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_18__shared_services_image__["a" /* ImageProvider */],
            __WEBPACK_IMPORTED_MODULE_22__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_19__shared_services_preloader__["a" /* PreloaderProvider */],
            __WEBPACK_IMPORTED_MODULE_25__shared_services_register_service__["a" /* RegisterService */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(254);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(256);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_auth__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_auth_provider__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_products_products__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_add_gym_add_gym__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_image_home__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_fitness_centers_fitness_centers__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_wellness_centers_wellness_center__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_firebase__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, auth, Auth) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.auth = auth;
        this.Auth = Auth;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        this.currentUser = [];
        this.rawList = [];
        this.userResponse = [];
        this.initializeApp();
        this.currentUser = __WEBPACK_IMPORTED_MODULE_13_firebase__["database"]().ref('/user-profiles');
        this.getCurrentUser();
        auth.authState.subscribe(function (authState) {
            _this.CurrentUserEmail = authState.email;
            console.log('Logged in user :', _this.CurrentUserEmail);
            if (authState) {
                console.log('Logged in user :', authState.uid);
            }
        });
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Edit Profile', component: __WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__["a" /* ProfilePage */] },
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_7__pages_products_products__["a" /* ProductsPage */] },
            // { title: 'Images', component: ImagePage },
            { title: 'Fitness Centers', component: __WEBPACK_IMPORTED_MODULE_10__pages_fitness_centers_fitness_centers__["a" /* FitnessCentersPage */] },
            { title: 'Wellness Centers', component: __WEBPACK_IMPORTED_MODULE_11__pages_wellness_centers_wellness_center__["a" /* WellnessCenterPage */] },
            { title: 'Personal Trainers', component: __WEBPACK_IMPORTED_MODULE_7__pages_products_products__["a" /* ProductsPage */] },
            { title: 'Meal Preps', component: __WEBPACK_IMPORTED_MODULE_7__pages_products_products__["a" /* ProductsPage */] },
        ];
        this.admin = [
            { title: 'Add-Personal-Trainer', component: __WEBPACK_IMPORTED_MODULE_8__pages_add_gym_add_gym__["a" /* AddGymPage */] },
            { title: 'Add-Fitness-Centers', component: __WEBPACK_IMPORTED_MODULE_9__pages_image_home__["a" /* ImagePage */] },
            { title: 'Add-Wellness-Centers', component: __WEBPACK_IMPORTED_MODULE_9__pages_image_home__["a" /* ImagePage */] },
            { title: 'Add-Meal-Preps', component: __WEBPACK_IMPORTED_MODULE_8__pages_add_gym_add_gym__["a" /* AddGymPage */] },
        ];
        this.settings = [
            { title: 'Log Out', component: __WEBPACK_IMPORTED_MODULE_9__pages_image_home__["a" /* ImagePage */] },
        ];
    }
    MyApp.prototype.logout = function () {
        this.Auth.logout();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.editProfile = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__["a" /* ProfilePage */]);
    };
    MyApp.prototype.getCurrentUser = function () {
        var _this = this;
        this.currentUser.on('value', function (expensecategories) {
            expensecategories.forEach(function (spanshot) {
                var cat = spanshot.val();
                _this.rawList.push({
                    $key: spanshot.key,
                    firstname: cat.firstname,
                    lastname: cat.lastname,
                    username: cat.username,
                    phonenumber: cat.phonenumber,
                    image: cat.image,
                    userId: cat.userId,
                });
            });
            _this.userResponse = _this.rawList;
        });
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/stella/PycharmProjects/ionic-starter/src/app/app.html"*/'<!--<ion-menu [content]="content">-->\n  <!--<ion-header transparent>-->\n    <!--<ion-toolbar transparent>-->\n      <!--<ion-title *ngIf="Auth.currentUser" transparent>Welcome {{Auth.currentUser}}</ion-title>-->\n    <!--</ion-toolbar>-->\n  <!--</ion-header>-->\n\n  <!--<ion-content style="color: #f7f7f7; background-color: #363738;">-->\n    <!--<ion-list transparent>-->\n       <!--<ion-item-divider color="default">Staff Content</ion-item-divider>-->\n      <!--<button menuClose color="light" block outline ion-item *ngFor="let p of pages" transparent (click)="openPage(p)">-->\n        <!--{{p.title}}-->\n      <!--</button>-->\n    <!--</ion-list>-->\n\n    <!--<ion-list transparent>-->\n      <!--<ion-item-divider color="default">Settings</ion-item-divider>-->\n      <!--<button menuClose color="light" block outline ion-item transparent (click)="logout()">-->\n        <!--Log out-->\n      <!--</button>-->\n    <!--</ion-list>-->\n\n  <!--</ion-content>-->\n\n<!--</ion-menu>-->\n\n<!--&lt;!&ndash; Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus &ndash;&gt;-->\n<!--<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>-->\n\n\n<ion-menu [content]="content" >\n\n  <ion-content #menu class="menu" color="dark" class="menu-bg" *ngIf="userResponse && CurrentUserEmail" >\n\n    <ion-card text-center class="hide-card card-color" *ngFor="let user of userResponse" >\n      <div *ngIf="user.userId == CurrentUserEmail">\n      <br>\n      <img [src]="user.image" class="custom-avatar" />\n      <br>\n      <h2 style="color: white">{{user.username}}</h2>\n      <p style="color: white">{{user.userId}}</p>\n      </div>\n    <hr>\n  </ion-card>\n\n    <ion-item-group color="dark" class="menu-bg" >\n      <ion-item-divider class="card-color">Users</ion-item-divider>\n      <button  menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)" class="menu-bg" >\n        {{p.title}}\n      </button>\n    </ion-item-group>\n\n    <ion-item-group color="dark" >\n        <ion-item-divider class="card-color">Admin</ion-item-divider>\n        <button class="menu-bg" color="dark" menuClose ion-item *ngFor="let p of admin" (click)="openPage(p)">\n          <!--<ion-icon [name]="p.icon" item-left></ion-icon>-->\n          {{p.title}}\n        </button>\n      </ion-item-group>\n\n    <ion-item-group color="dark">\n        <ion-item-divider class="card-color">Settings</ion-item-divider>\n        <button class="menu-bg"  color="dark" menuClose ion-item (click)="logout()">\n          <!--<ion-icon [name]="p.icon" item-left></ion-icon>-->\n          Log Out\n        </button>\n      </ion-item-group>\n\n\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n<!--ionic cordova run android --device-->\n'/*ion-inline-end:"/Users/stella/PycharmProjects/ionic-starter/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
        __WEBPACK_IMPORTED_MODULE_5_angularfire2_auth__["a" /* AngularFireAuth */],
        __WEBPACK_IMPORTED_MODULE_6__providers_auth_provider__["a" /* AuthProvider */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__products_products__ = __webpack_require__(80);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SignupPage = (function () {
    function SignupPage(navCtrl, navParams, fb, auth) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.auth = auth;
        this.signupForm = this.fb.group({
            'email': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)])],
            'password': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].minLength(1)])]
        });
        this.email = this.signupForm.controls['email'];
        this.password = this.signupForm.controls['password'];
    }
    SignupPage.prototype.submit = function () {
        var _this = this;
        if (this.signupForm.valid) {
            var credentials = ({ email: this.email.value, password: this.password.value });
            this.auth.registerUser(credentials).subscribe(function (registerData) {
                console.log(registerData);
                alert('User is registered and logged in.');
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__products_products__["a" /* ProductsPage */]);
            }, function (registerError) {
                console.log(registerError);
                if (registerError.code === 'auth/weak-password' || registerError.code === 'auth/email-already-in-use') {
                    alert(registerError.message);
                }
                _this.error = registerError;
            });
        }
    };
    return SignupPage;
}());
SignupPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-signup',template:/*ion-inline-start:"/Users/stella/PycharmProjects/ionic-starter/src/pages/register/register.html"*/'<ion-header no-border="">\n  <ion-navbar color="primary" transparent>\n    <!--<ion-title>Login to Tunaweza</ion-title>-->\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div class="dark-gradient"></div>\n   <ion-grid fixed class="padding-top">\n        <ion-row>\n            <ion-col col-lg-6 push-lg-3 col-md-6 push-md-3 col-sm-12>\n              <!--<ion-title>Signup</ion-title>-->\n              <form [formGroup]="signupForm" (ngSubmit)="submit()" novalidate>\n                  <ion-row>\n                    <ion-item>\n                        <ion-label for="email"></ion-label>\n                        <ion-input type="email" value="" placeholder="Email" formControlName="email"></ion-input>\n                    </ion-item>\n                  </ion-row>\n                  <ion-row>\n                    <ion-item>\n                        <ion-label for="password"></ion-label>\n                        <ion-input type="password" placeholder="Password" formControlName="password"></ion-input>\n                    </ion-item>\n                  </ion-row>\n                <br><br>\n\n                  <ion-row>\n                      <button class="register-button" ion-button type="submit" color="light" block outline>Sign up</button>\n                  </ion-row>\n              </form>\n            </ion-col>\n        </ion-row>\n   </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/ionic-starter/src/pages/register/register.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__["a" /* AuthProvider */]])
], SignupPage);

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 80:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signup_signup__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__reset_password_reset_password__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__list_gym_list_gym__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__fitness_centers_fitness_centers__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__wellness_centers_wellness_center__ = __webpack_require__(159);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








 //Added reset password page
var ProductsPage = (function () {
    function ProductsPage(navCtrl, fb, auth) {
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.auth = auth;
        this.signupPage = __WEBPACK_IMPORTED_MODULE_4__signup_signup__["a" /* SignupPage */];
        this.resetPasswordPage = __WEBPACK_IMPORTED_MODULE_5__reset_password_reset_password__["a" /* ResetPasswordPage */]; //Added reset password page
    }
    ProductsPage.prototype.goToGym = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__fitness_centers_fitness_centers__["a" /* FitnessCentersPage */]);
    };
    ProductsPage.prototype.goToPersonalTrainer = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__list_gym_list_gym__["a" /* ListGymPage */]);
    };
    ProductsPage.prototype.goToWellnessCenterPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__wellness_centers_wellness_center__["a" /* WellnessCenterPage */]);
    };
    return ProductsPage;
}());
ProductsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-products',template:/*ion-inline-start:"/Users/stella/PycharmProjects/ionic-starter/src/pages/products/products.html"*/'<ion-header no-border="">\n  <ion-navbar transparent>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding >\n    <div class="dark-gradient"></div>\n<ion-grid style="margin-top: 100px">\n  <ion-row  (tap)="goToGym()">\n      <ion-col col-lg-12 push-lg-0 col-md-12 push-md-0 no-padding>\n        <div>\n          <ion-row no-padding class="discovery-row">\n            <ion-col class="discovery-col" col-xl-12 col-lg-12 col-md-12 col-12 no-padding >\n              <div class="discovery-card">\n               <br>\n                <h1>Fitness Centers</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe"></div>\n              </div>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-col>\n    </ion-row>\n\n  <ion-row (tap)="goToPersonalTrainer()" >\n      <ion-col col-lg-12 push-lg-0 col-md-12 push-md-0 no-padding>\n        <div>\n          <ion-row no-padding class="discovery-row">\n            <ion-col class="discovery-col" col-xl-12 col-lg-12 col-md-12 col-12 no-padding >\n              <div class="discovery-card">\n                <br>\n                <h1>Personal Trainers</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe2"></div>\n              </div>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-col>\n    </ion-row>\n\n  <ion-row  (tap)="goToWellnessCenterPage()">\n      <ion-col col-lg-12 push-lg-0 col-md-12 push-md-0 no-padding>\n        <div>\n          <ion-row no-padding class="discovery-row">\n            <ion-col class="discovery-col" col-xl-12 col-lg-12 col-md-12 col-12 no-padding >\n              <div class="discovery-card">\n                <br>\n                <h1>Wellness Centers</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe3"></div>\n              </div>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-col>\n    </ion-row>\n\n  <ion-row (tap)="goToPersonalTrainer()">\n      <ion-col col-lg-12 push-lg-0 col-md-12 push-md-0 no-padding>\n        <div>\n          <ion-row no-padding class="discovery-row">\n            <ion-col class="discovery-col" col-xl-12 col-lg-12 col-md-12 col-12 no-padding >\n              <div class="discovery-card">\n                <br>\n                <h1>Meal Preps</h1>\n                <p>Inspirational Stuff</p>\n                <div class="discovery-stripe4"></div>\n              </div>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-col>\n    </ion-row>\n\n\n  </ion-grid>\n\n\n\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/ionic-starter/src/pages/products/products.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__["a" /* AuthProvider */]])
], ProductsPage);

//# sourceMappingURL=products.js.map

/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(142);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ResetPasswordPage = (function () {
    function ResetPasswordPage(navCtrl, navParams, fb, auth) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.auth = auth;
        this.resetPasswordForm = this.fb.group({
            'email': ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)])]
        });
        this.email = this.resetPasswordForm.controls['email'];
    }
    ResetPasswordPage.prototype.submit = function () {
        var _this = this;
        if (this.resetPasswordForm.valid) {
            this.auth.resetPassword(this.email.value).subscribe(function (registerData) {
                alert('Password recovery link is sent.');
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
            }, function (registerError) {
                console.log(registerError);
                if (registerError.code === 'auth/user-not-found') {
                    alert(registerError.message);
                }
            });
        }
    };
    return ResetPasswordPage;
}());
ResetPasswordPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-reset-password',template:/*ion-inline-start:"/Users/stella/PycharmProjects/ionic-starter/src/pages/reset-password/reset-password.html"*/'<ion-header no-border="">\n  <ion-navbar transparent="">\n    <!--<ion-title>Ionic2Firebase</ion-title>-->\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div class="dark-gradient"></div>\n\n  <form [formGroup]="resetPasswordForm" (ngSubmit)="submit()" novalidate class="padding-top">\n    <ion-row>\n      <ion-item>\n        <ion-label for="email"></ion-label>\n        <ion-input type="email" value="" placeholder="Email" formControlName="email"></ion-input>\n    </ion-item>\n    </ion-row>\n    <br><br>\n    <ion-row>\n      <button ion-button class="login-button" color="light" type="submit" block outline>Reset password</button>\n    </ion-row>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/ionic-starter/src/pages/reset-password/reset-password.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3__providers_auth_provider__["a" /* AuthProvider */]])
], ResetPasswordPage);

//# sourceMappingURL=reset-password.js.map

/***/ }),

/***/ 82:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListGymPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_provider__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__view_gym_view_gym__ = __webpack_require__(314);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ListGymPage = (function () {
    function ListGymPage(navCtrl, navParams, af, auth, _toastCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.af = af;
        this.auth = auth;
        this._toastCtrl = _toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.GymData = [];
        this.rawList = [];
        this.gymResponse = [];
        this.loading = true;
        this.filter = [];
        this.GymData = __WEBPACK_IMPORTED_MODULE_4_firebase__["database"]().ref('/fitness-centers');
        this.getGymData();
    }
    ListGymPage.prototype.showToast = function (msg, duration) {
        if (duration === void 0) { duration = 4000; }
        var toast = this._toastCtrl.create({
            message: msg,
            duration: duration
        });
    };
    ListGymPage.prototype.showLoading = function () {
        this.loadingController = this.loadingCtrl.create({
            content: 'Loading'
        });
        this.loadingController.present();
    };
    ListGymPage.prototype.hideLoading = function () {
        this.loadingController.dismiss();
    };
    ListGymPage.prototype.getGymData = function () {
        var _this = this;
        this.showLoading();
        this.GymData.on('value', function (expensecategories) {
            expensecategories.forEach(function (spanshot) {
                var cat = spanshot.val();
                _this.rawList.push({
                    $key: spanshot.key,
                    title: cat.title,
                    location: cat.location,
                    category: cat.category,
                    image: cat.image,
                    price: cat.price,
                    // phone: cat.phone,
                    // email: cat.email,
                    description: cat.description
                });
            });
            _this.gymResponse = _this.rawList;
            console.log('gymResponse', _this.gymResponse);
            _this.hideLoading();
        });
    };
    ListGymPage.prototype.goTogym = function (data) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__view_gym_view_gym__["a" /* GymPage */], {
            gymId: data.$key
        });
    };
    ListGymPage.prototype.initializeItems = function () {
        this.filter = this.gymResponse;
    };
    ListGymPage.prototype.getItems = function (searchbar) {
        var _this = this;
        var q = searchbar.srcElement.value;
        this.gymResponse = this.gymResponse.filter(function (v) {
            if (v.title && q) {
                if (v.title.toLowerCase().indexOf(q.toLowerCase()) > -1) {
                    return true;
                }
                return false;
            }
            if (!v.title && !q) {
                _this.getGymData();
                return;
            }
        });
    };
    return ListGymPage;
}());
ListGymPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-list-gym',template:/*ion-inline-start:"/Users/stella/PycharmProjects/ionic-starter/src/pages/list-gym/list-gym.html"*/'<ion-header no-border="">\n  <ion-navbar transparent>\n    <button ion-button menuToggle style="color: #fff">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title></ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding *ngIf="loading">\n  <div class="dark-gradient"></div>\n  <ion-list class="padding-top">\n    <ion-searchbar placeholder="Search " (ionInput)="getItems($event)"></ion-searchbar>\n  </ion-list>\n\n  <ion-list  *ngIf="gymResponse">\n  <ion-item *ngFor="let data of gymResponse">\n    <ion-thumbnail item-start>\n      <img [src]="data.image">\n      <!--<pre>{{data | json}}</pre>-->\n    </ion-thumbnail>\n    <h2>{{data.title}}</h2>\n    <p style="color: #f7f7f7" *ngFor="let location of data.location">\n      {{location.name}}</p>\n    <button ion-button clear item-end (tap)="goTogym(data)">View</button>\n  </ion-item>\n</ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/stella/PycharmProjects/ionic-starter/src/pages/list-gym/list-gym.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_2__providers_auth_provider__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* LoadingController */]])
], ListGymPage);

//# sourceMappingURL=list-gym.js.map

/***/ })

},[321]);
//# sourceMappingURL=main.js.map